// ProUnkno.h: interface for the CProUnknown class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROUNKNO_H__2277D120_B6A1_47A3_BF78_EBF609497E1D__INCLUDED_)
#define AFX_PROUNKNO_H__2277D120_B6A1_47A3_BF78_EBF609497E1D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Pro.h"

class CProUnknown : public CPro  
{
public:
	CProUnknown();
	CProUnknown(CMapomDoc* pDoc);
	virtual ~CProUnknown();

    int    LL2x  (CGeoPoint GP);
    int    LL2y  (CGeoPoint GP);
    double xy2Lat(int x, int y);
    double xy2Lon(int x, int y);

//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated);
//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3);
//        void OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS);

//        double CProUnknown::LL2z(CGeoPoint GP);
//        double CProUnknown::wz2Lat(double w, double z);
};

#endif // !defined(AFX_PROUNKNO_H__2277D120_B6A1_47A3_BF78_EBF609497E1D__INCLUDED_)
