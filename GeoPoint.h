// GeoPoint.h: interface for the CGeoPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GEOPOINT_H__DEB6DC33_E002_4D05_B96C_9457EEB04538__INCLUDED_)
#define AFX_GEOPOINT_H__DEB6DC33_E002_4D05_B96C_9457EEB04538__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGeoPoint  
{
protected:
	double Lat;
	double Lon;

public:
	CGeoPoint();
//	CGeoPoint(float lat, float lon);
	CGeoPoint(double lat, double lon);
//	CGeoPoint(CGeoPoint GeoPoint);
	virtual ~CGeoPoint();
	CString GetUTM();
	BOOL Is_N();
	BOOL Is_S();
	BOOL Is_W();
	BOOL Is_E();
	double GetLon();
	double GetLat();
	unsigned int GetDegLon();
	unsigned int GetDegLat();
	double GetMinLon();
	double GetMinLat();
	unsigned int GetMintLon();
	unsigned int GetMintLat();
	double GetSecLon();
	double GetSecLat();
};

#endif // !defined(AFX_GEOPOINT_H__DEB6DC33_E002_4D05_B96C_9457EEB04538__INCLUDED_)
