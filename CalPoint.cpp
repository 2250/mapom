// CalPoint.cpp: implementation of the CCalPoint class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "CalPoint.h"
#include "mapomDoc.h"
#include "mapoView.h"
//#include "string.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCalPoint::CCalPoint()
{
	Point.x=0;
	Point.y=0;
	UseIt=false;
}

CCalPoint::CCalPoint(CPoint Pt)
{
	Point=Pt;
	UseIt=false;
}

CCalPoint::CCalPoint(CGeoPoint GeoPoint) : CGeoPoint(GeoPoint)
{
//	Lat=GeoPoint.GetLat();
//	Lon=GeoPoint.GetLon();
	UseIt=false;
}

CCalPoint::CCalPoint(CPoint Pt, CGeoPoint GeoPoint) : CGeoPoint(GeoPoint)
{
	Point=Pt;
//	Lat=GeoPoint.GetLat();
//	Lon=GeoPoint.GetLon();
	UseIt=false;
}

CCalPoint::CCalPoint(int x, int y, float lat, float lon) : CGeoPoint(lat, lon)
{
	Point.x=x;
	Point.y=y;
	UseIt=false;
}

CCalPoint::~CCalPoint()
{
}

void CCalPoint::operator=(CCalPoint CalPoint)
{
	Point=CalPoint.Point;
	Lat=CalPoint.GetLat();
	Lon=CalPoint.GetLon();
	UseIt=CalPoint.Use();
}

CPoint CCalPoint::GetPoint()
{
	CPoint CPt=Point;
	return(CPt);
}

void CCalPoint::Serialize(CArchive& ar, int iPoint)
{
	double lat, lon, min;
	CString S;
	CString S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,S16,S17;
//	CString SA;
	if (ar.IsStoring())
	{
		if(UseIt)
		{
		S.Format("Point%02d, %s, %5d, %5d, %s, %s,  %2d, %06.3f, %c, %3d, %06.3f, %c, %s, %s, %s, %s, %c",
			iPoint, "xy", Point.x, Point.y, "in", "deg",
			GetDegLat(), GetMinLat(), Is_N()?'N':Is_S()?'S':' ',
			GetDegLon(), GetMinLon(), Is_W()?'W':Is_E()?'E':' ',
			"grid", "", "", "", 'N');
		}
		else
		{
		S.Format("Point%02d, %s, %s , %s , %s, %s,  %s , %s    , %c, %s , %s    , %c, %s, %s, %s, %s, %c",
			iPoint, "xy",   "   ",   "   ", "in", "deg",
			" "        , "   "      ,        'N'               ,
			"  "       , "   "      ,        'W'               ,
			"grid", "", "", "", 'N');
		}
		ar.WriteString((LPCTSTR)(S+"\r\n"));
	}
	else
	{
		ar.ReadString(S);
		sscanf((LPCTSTR)S,"Point%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^',']",
			S1 .GetBufferSetLength(255),
			S2 .GetBufferSetLength(255),
			S3 .GetBufferSetLength(255),
			S4 .GetBufferSetLength(255),
			S5 .GetBufferSetLength(255),
			S6 .GetBufferSetLength(255),

			S7 .GetBufferSetLength(255),
			S8 .GetBufferSetLength(255),
			S9 .GetBufferSetLength(255),

			S10.GetBufferSetLength(255),
			S11.GetBufferSetLength(255),
			S12.GetBufferSetLength(255),

			S13.GetBufferSetLength(255),
			S14.GetBufferSetLength(255),
			S15.GetBufferSetLength(255),
			S16.GetBufferSetLength(255),
			S17.GetBufferSetLength(255));
//		int i;
		UseIt=true;

		if(atoi(S1)!=iPoint)
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		S2.TrimLeft();
		S2.TrimRight();
		if(S2!="xy")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		S3.TrimLeft();
		S3.TrimRight();
		S4.TrimLeft();
		S4.TrimRight();
		if(S3==""||S4==""){UseIt=false;}
		Point.x=atoi(S3);
		Point.y=atoi(S4);

		S5.TrimLeft();
		S5.TrimRight();
		S6.TrimLeft();
		S6.TrimRight();
		if((S5!="in"&&S5!="")||(S6!="deg"&&S6!=""))
		{// bad format
			MessageBox(NULL,"unknown *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		// Latitude
		S7.TrimLeft();
		S7.TrimRight();
		S8.TrimLeft();
		S8.TrimRight();
		if(S7==""&&S8==""){UseIt=false;}
		lat=min=0;
		if(0>(lat=atoi(S7)))
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}
		if(0>(min=atof(S8))||60<=min)
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}
		if(90<(lat+=min/60))
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		S9.TrimLeft();
		S9.TrimRight();
		if(S9=="S"||S9=="s")
		{lat=-lat;}
		else if(S9!="N"&&S9!="n"&&S9!="")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		// Longitude
		S10.TrimLeft();
		S10.TrimRight();
		S11.TrimLeft();
		S11.TrimRight();
		if(S10==""&&S11==""){UseIt=false;}
		lon=min=0;
		if(0>(lon=atoi(S10)))
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}
		if(0>(min=atof(S11))||60<=min)
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}
		if(180<(lon+=min/60))
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		S12.TrimLeft();
		S12.TrimRight();
		if(S12=="W"||S12=="w")
		{lon=-lon;}
		else if(S12!="E"&&S12!="e"&&S12!="")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		Lat=(lat>90?90:lat<-90?-90:lat);
		Lon=lon-360*(int)((lon+180)/360);
		Lon+=(Lon<-180?360:0);

		S13.TrimLeft();
		S13.TrimRight();
		if(S13!="grid"&&S13!="")
		{// bad format
			MessageBox(NULL,"unknown *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}

		//UNKNOWN PARAMETER
		S14.TrimLeft();
		S14.TrimRight();
		//UNKNOWN PARAMETER
		S15.TrimLeft();
		S15.TrimRight();
		//UNKNOWN PARAMETER
		S16.TrimLeft();
		S16.TrimRight();

		//UNKNOWN PARAMETER
		S17.TrimLeft();
		S17.TrimRight();
		if(S17=="S"||S17=="s")
		{/*lat=-lat;*/}
		else if(S17!="N"&&S17!="n"&&S17!="")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			UseIt=false;
		}
	}
	return;
}

void CCalPoint::OnDraw(CDC *pDC, CMapomDoc *pDoc, bool ClientCS)
{
	CPoint pt, pt1;
	pt1=GetPoint();
	pDC->SelectObject(pDoc->pen0);
	pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
	int r=4;
	pDC->Arc(pt.x-r+1,pt.y-r+1,pt.x+r,pt.y+r,pt.x+r,pt.y+r,pt.x+r,pt.y+r);
	r=10;
	pDC->Arc(pt.x-r+1,pt.y-r+1,pt.x+r,pt.y+r,pt.x+r,pt.y+r,pt.x+r,pt.y+r);
	r=16;
	pDC->Arc(pt.x-r+1,pt.y-r+1,pt.x+r,pt.y+r,pt.x+r,pt.y+r,pt.x+r,pt.y+r);
	r=17;
	pDC->Arc(pt.x-r+1,pt.y-r+1,pt.x+r,pt.y+r,pt.x+r,pt.y+r,pt.x+r,pt.y+r);
//	pDC->Arc(pt.x-15,pt.y-15,pt.x+15+1,pt.y+15+1,pt.x+15+1,pt.y+15+1,pt.x+15+1,pt.y+15+1);
//	pDC->Arc(pt.x-16,pt.y-16,pt.x+16+1,pt.y+16+1,pt.x+16+1,pt.y+16+1,pt.x+16+1,pt.y+16+1);
	return;
}

bool CCalPoint::Use(){return(UseIt);}

void CCalPoint::Use(bool setUseIt){UseIt=setUseIt;}
