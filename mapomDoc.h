// mapomDoc.h : interface of the CMapomDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPOMDOC_H__AB9EEEE1_6918_4662_BE3F_7859D4BB4ECE__INCLUDED_)
#define AFX_MAPOMDOC_H__AB9EEEE1_6918_4662_BE3F_7859D4BB4ECE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <sys/timeb.h>
#include <time.h>
#include "MyMap.h"
#include "Pro.h"
#include "ProLatLo.h"
#include "ProMerca.h"
#include "ProAzEqd.h"
#include "ProUnkno.h"

//#include "CalibDlg.h"
class CMapomView;
class CMainFrame;

class CMapomDoc : public CDocument
{
protected: // create from serialization only
    CMapomDoc();
    DECLARE_DYNCREATE(CMapomDoc)

// Attributes
public:
    int    DocCreTime;
    int    DocModTime;
//	CMyMap MyMap;
	CMyMap *pMyMap;
	CPro *pPro;

    CPoint StatusXY;

//    CSize DocSz  ;  // page setup
//    int   scale  ;  // screen
//    CSize shift  ;  // screen
    CPen*pen0; //
    CPen*pen1; //
    CBrush*pShadowBrush;
    BOOL fast_draw;  // fast draw method
    bool redraw_all; // fast draw method

// Operations
public:
    void SetPens();
    CMapomView* GetView();
    CMainFrame* GetMainFrame();


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMapomDoc)
    public:
    virtual BOOL OnNewDocument();
    virtual void Serialize(CArchive& ar);
    //}}AFX_VIRTUAL

// Implementation
public:
    int MakeCoefs();
    virtual ~CMapomDoc();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
    //{{AFX_MSG(CMapomDoc)
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPOMDOC_H__AB9EEEE1_6918_4662_BE3F_7859D4BB4ECE__INCLUDED_)
