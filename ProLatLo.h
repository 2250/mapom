// ProLatLo.h: interface for the CProLatLon class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROLATLO_H__9BD94160_4078_11D9_9098_BD1A27D0DA63__INCLUDED_)
#define AFX_PROLATLO_H__9BD94160_4078_11D9_9098_BD1A27D0DA63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Pro.h"

class CProLatLon : public CPro  
{
//protected:
//    double k[6];
//    double r[6];

public:
        CProLatLon();
		CProLatLon(CMapomDoc* pDoc);
        virtual ~CProLatLon();

        int    LL2x  (CGeoPoint GP);
        int    LL2y  (CGeoPoint GP);
        double xy2Lat(int x, int y);
        double xy2Lon(int x, int y);

//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated);
//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3);
        void OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS);
};

#endif // !defined(AFX_PROLATLO_H__9BD94160_4078_11D9_9098_BD1A27D0DA63__INCLUDED_)
