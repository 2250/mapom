// ProAzEqd.cpp: implementation of the CProAzEqd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "ProAzEqd.h"

#include "mapomDoc.h"
#include "mapoView.h"
#include "math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProAzEqd::CProAzEqd()
{
        ProType=azeqd; // see: Pro.h
}

CProAzEqd::CProAzEqd(CMapomDoc* pDoc)
{
        ProType=azeqd; // see: Pro.h
    this->pDoc=pDoc;
}

CProAzEqd::~CProAzEqd()
{

}

double CProAzEqd::LL2w(CGeoPoint GP)
{return((90-GP.GetLat())*cos(RAD(GP.GetLon())));
}

double CProAzEqd::LL2z(CGeoPoint GP)
{return((90-GP.GetLat())*sin(RAD(GP.GetLon())));
}

double CProAzEqd::wz2Lat(double w, double z)
{return(CGeoPoint(90-sqrt(z*z+w*w),0).GetLat());
}

double CProAzEqd::wz2Lon(double w, double z)
{return(CGeoPoint(0,DEG((w>0||w<0)?atan(z/w)+(w>0?0:3.1415926):3.1415926*(z>0?1:-1))).GetLon());
}

int CProAzEqd::LL2x(CGeoPoint GP)
{double zzzzzzzzzzz=LL2z(GP);
 double wwwwwwwwwww=LL2w(GP);
 return((int)(r[0]*wwwwwwwwwww+r[1]*zzzzzzzzzzz+r[2]));
//return((int)(r[0]*GP.GetLon()+r[1]*GP.GetLat()+r[2]));
}

int CProAzEqd::LL2y(CGeoPoint GP)
{double zzzzzzzzzzz=LL2z(GP);
 double wwwwwwwwwww=LL2w(GP);
 return((int)(r[3]*wwwwwwwwwww+r[4]*zzzzzzzzzzz+r[5]));
//return((int)(r[3]*GP.GetLon()+r[4]*GP.GetLat()+r[5]));
}

double CProAzEqd::xy2Lat(int x, int y)
{double wwwwwwwwwww=
       (      k[0]*x          +k[1]*y          +k[2] );
 double zzzzzzzzzzz=
       (      k[3]*x          +k[4]*y          +k[5] );
 return(wz2Lat(wwwwwwwwwww, zzzzzzzzzzz));
//return(      k[3]*x          +k[4]*y          +k[5] );
}

double CProAzEqd::xy2Lon(int x, int y)
{double wwwwwwwwwww=
       (      k[0]*x          +k[1]*y          +k[2] );
 double zzzzzzzzzzz=
       (      k[3]*x          +k[4]*y          +k[5] );
 return(wz2Lon(wwwwwwwwwww, zzzzzzzzzzz));
//return(      k[0]*x          +k[1]*y          +k[2] );
}

/*
void CProAzEqd::OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS)
{
    CPoint pt, pt1,pt2;

    double min_lat, max_lat;
    double min_lon, max_lon;
    double dbl, db2, db3;
        CSize Sz=pDoc->MyMap.GetSize();

    dbl=xy2Lat(    0,     0);
    min_lat=             dbl         ;
    max_lat=              90         ;
//    dbl=xy2Lon(    0,     0);
    min_lon=            -180         ;
    max_lon=             180         ;

    dbl=xy2Lat(Sz.cx,     0);
    min_lat=(dbl<min_lat?dbl:min_lat);
//    max_lat=(dbl>max_lat?dbl:max_lat);
//    dbl=xy2Lon(Sz.cx,     0);
//    min_lon=(dbl<min_lon?dbl:min_lon);
//    max_lon=(dbl>max_lon?dbl:max_lon);

    dbl=xy2Lat(    0, Sz.cy);
    min_lat=(dbl<min_lat?dbl:min_lat);
//    max_lat=(dbl>max_lat?dbl:max_lat);
//    dbl=xy2Lon(    0, Sz.cy);
//    min_lon=(dbl<min_lon?dbl:min_lon);
//    max_lon=(dbl>max_lon?dbl:max_lon);

    dbl=xy2Lat(Sz.cx, Sz.cy);
    min_lat=(dbl<min_lat?dbl:min_lat);
//    max_lat=(dbl>max_lat?dbl:max_lat);
//    dbl=xy2Lon(Sz.cx, Sz.cy);
//    min_lon=(dbl<min_lon?dbl:min_lon);
//    max_lon=(dbl>max_lon?dbl:max_lon);

    min_lat=(-90>min_lat?-90:min_lat);
    max_lat=( 90<max_lat? 90:max_lat);

    dbl=log(max_lat-min_lat)/log(10);
        db2=1/60.0;
    if(dbl>-1.25){db2=1/30.0;}
    if(dbl>-1.00){db2=1/20.0;}
    if(dbl>-0.75){db2=1/15.0;}
    if(dbl>-0.50){db2=1/12.0;}
    if(dbl>-0.25){db2=1/10.0;}
    if(dbl> 0.00){db2=1/ 6.0;}
    if(dbl> 0.25){db2=1/ 4.0;}
    if(dbl> 0.45){db2=1/ 3.0;}
    if(dbl> 0.65){db2=1/ 2.0;}
    if(dbl> 0.85){db2= 1;}
    if(dbl> 1.05){db2= 2;}
    if(dbl> 1.25){db2= 4;}
    if(dbl> 1.45){db2= 5;}
    if(dbl> 1.65){db2=10;}
    if(dbl> 1.85){db2=15;}
    if(dbl> 2.05){db2=20;}
    if(dbl> 2.25){db2=30;}

        pDC->SelectObject(pDoc->pen1);

    for(db3=db2*(int)(min_lat/db2)+(min_lat<0?0:db2);db3<max_lat;db3+=db2)
    {
        pt1=LL2xy(db3,min_lon);
                pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
                pDC->MoveTo(pt.x, pt.y);

      for(double db4=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db4<=max_lon;db4+=db2)
      {
        pt2=LL2xy(db3,db4);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
      }
    }

    for(db3=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db3<max_lon;db3+=db2)
    {
        pt1=LL2xy(min_lat,db3);
        pt2=LL2xy(max_lat,db3);
                pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
                pDC->MoveTo(pt.x, pt.y);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
    }

        pDC->SelectObject(pDoc->pen0);

// equator:
    pt1=LL2xy(  0,min_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);

      for(double db4=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db4<=max_lon;db4+=db2)
      {
        pt2=LL2xy(db3,db4);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
      }

//    pt2=LL2xy(  0,max_lon);
  //      pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    //pDC->LineTo(pt.x, pt.y);

// 90 S:
//    pt1=LL2xy(-90,min_lon);
  //  pt2=LL2xy(-90,max_lon);
    //    pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
//    pDC->MoveTo(pt.x, pt.y);
  //      pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    //pDC->LineTo(pt.x, pt.y);

// 90 N:
//    pt1=LL2xy( 90,min_lon);
  //  pt2=LL2xy( 90,max_lon);
    //    pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
//    pDC->MoveTo(pt.x, pt.y);
  //      pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    //pDC->LineTo(pt.x, pt.y);

// Greenwich:
        pDC->SelectObject(pDoc->pen0);

    pt1=LL2xy(-90, 360);
    pt2=LL2xy( 90, 360);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

    pt1=LL2xy(-90,   0);
    pt2=LL2xy( 90,   0);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

    pt1=LL2xy(-90,-360);
    pt2=LL2xy( 90,-360);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// 180 E == 180 W:
    pt1=LL2xy(-90, 180);
    pt2=LL2xy( 90, 180);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// 180 W == 180 E:
    pt1=LL2xy(-90,-180);
    pt2=LL2xy( 90,-180);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

        return;
}
*/
