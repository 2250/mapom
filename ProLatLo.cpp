// ProLatLo.cpp: implementation of the CProLatLon class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "ProLatLo.h"

#include "mapomDoc.h"
#include "mapoView.h"
#include "math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProLatLon::CProLatLon()
{
    ProType=latlon; // see: Pro.h
//        for(int i=0;i<6;i++)
//        {
//                k[i]=r[0]=0;
//        }
}

CProLatLon::CProLatLon(CMapomDoc* pDoc)
{
    ProType=latlon; // see: Pro.h
    this->pDoc=pDoc;
}

CProLatLon::~CProLatLon()
{

}

int CProLatLon::LL2x(CGeoPoint GP)
{return((int)(r[0]*GP.GetLon()+r[1]*GP.GetLat()+r[2]));
}

int CProLatLon::LL2y(CGeoPoint GP)
{return((int)(r[3]*GP.GetLon()+r[4]*GP.GetLat()+r[5]));
}

double CProLatLon::xy2Lat(int x, int y)
{return(      k[3]*x          +k[4]*y          +k[5] );
}

double CProLatLon::xy2Lon(int x, int y)
{return(      k[0]*x          +k[1]*y          +k[2] );
}

/*
bool CProLatLon::Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated)
{
        double g[3], h[3]; // x,y
        double p[3], q[3]; // long,lat
        double D, d, D0, D1, D2, D3;

        double s[6];

        g[0]=CP1.GetPoint().x;
        h[0]=CP1.GetPoint().y;
        q[0]=CP1.GetLat  ()  ;
        p[0]=CP1.GetLon  ()  ;
        g[1]=CP2.GetPoint().x;
        h[1]=CP2.GetPoint().y;
        q[1]=CP2.GetLat  ()  ;
        p[1]=CP2.GetLon  ()  ;

        double dg=g[1]-g[0];
        double dh=h[1]-h[0];
        double dp=p[1]-p[0];
        double dq=q[1]-q[0];
        double dd=g[0]*h[1]-g[1]*h[0];

        if(rotated)
        {

                D =                  -dg*dg                   -dh*dh;
                if(D==0) return(false);
                D0=                  -dp*dg                   -dq*dh;
                D1=                   dq*dg                   -dp*dh;
                D2=(p[1]*g[0]-p[0]*g[1])*dg+(p[1]*h[0]-p[0]*h[1])*dh+dd*dq;
                D3=(q[1]*g[0]-q[0]*g[1])*dg+(q[1]*h[0]-q[0]*h[1])*dh-dd*dp;

                k[0]=(D0/D);
                k[1]=(D1/D);
                k[2]=(D2/D);
                k[3]=-k[1];
                k[4]= k[0];
                k[5]=(D3/D);
        }
        else
        { // normal-oriented
                D =-dg*dh;
                if(D==0) return(false);
                D0=                  -dp*dh;
                D1=(p[1]*g[0]-p[0]*g[1])*dh;
                D2=                  -dq*dg;
                D3=(q[1]*h[0]-q[0]*h[1])*dg;

                k[0]=(D0/D);
                k[1]=0;
                k[2]=(D1/D);
                k[3]=0;
                k[4]=(D2/D);
                k[5]=(D3/D);
        }

        // ====

        g[0]=CP1.GetLat  ()  ;
        h[0]=CP1.GetLon  ()  ;
        p[0]=CP1.GetPoint().x;
        q[0]=CP1.GetPoint().y;
        g[1]=CP2.GetLat  ()  ;
        h[1]=CP2.GetLon  ()  ;
        p[1]=CP2.GetPoint().x;
        q[1]=CP2.GetPoint().y;

        dg=g[1]-g[0];
        dh=h[1]-h[0];
        dp=p[1]-p[0];
        dq=q[1]-q[0];
        dd=g[0]*h[1]-g[1]*h[0];

        if(rotated)
        {

                d =                  -dg*dg                   -dh*dh;
                if(d==0) return(false);
                D0=                  -dp*dg                   -dq*dh;
                D1=                   dq*dg                   -dp*dh;
                D2=(p[1]*g[0]-p[0]*g[1])*dg+(p[1]*h[0]-p[0]*h[1])*dh+dd*dq;
                D3=(q[1]*g[0]-q[0]*g[1])*dg+(q[1]*h[0]-q[0]*h[1])*dh-dd*dp;

                r[0]=(D0/d);
                r[1]=(D1/d);
                r[2]=(D2/d);
                r[3]=-r[1];
                r[4]= r[0];
                r[5]=(D3/d);

                s[0]=( k[0]*D/d);
                s[1]=(-k[1]*D/d);
        //      s[2]=;
                s[3]=(-s[1]    );
                s[4]=( s[0]    );
        //      s[5]=;
        }
        else
        { // normal-oriented
                d =-dg*dh;
                if(d==0) return(false);
                D0=                  -dp*dh;
                D1=(p[1]*g[0]-p[0]*g[1])*dh;
                D2=                  -dq*dg;
                D3=(q[1]*h[0]-q[0]*h[1])*dg;

                r[0]=(D0/d);
                r[1]=0;
                r[2]=(D1/d);
                r[3]=0;
                r[4]=(D2/d);
                r[5]=(D3/d);

                s[0]=( k[4]*D/d );
                s[1]= 0;
                //s[2]=g[0]-s[0]*p[0];
                s[2]=(-k[2]*s[0]);
                s[3]= 0;
                s[4]=( k[0]*D/d );
                //s[5]=h[0]-s[4]*q[0];
            s[5]=(-k[5]*s[4]);

        }
        return(true);
}

bool CProLatLon::Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3)
{
        double g[3], h[3]; // x,y
        double p[3], q[3]; // long,lat

        double s[6];

        g[0]=CP1.GetPoint().x;
        h[0]=CP1.GetPoint().y;
        q[0]=CP1.GetLat  ()  ;
        p[0]=CP1.GetLon  ()  ;
        g[1]=CP2.GetPoint().x;
        h[1]=CP2.GetPoint().y;
        q[1]=CP2.GetLat  ()  ;
        p[1]=CP2.GetLon  ()  ;
        g[2]=CP3.GetPoint().x;
        h[2]=CP3.GetPoint().y;
        q[2]=CP3.GetLat  ()  ;
        p[2]=CP3.GetLon  ()  ;

double z= g[0]*h[1]     -g[0]*h[2]     -g[1]*h[0]     +g[1]*h[2]     +g[2]*h[0]     -g[2]*h[1];
        if(z==0) return(false);
    k[0]=((p[0]*h[1]     -p[0]*h[2]     -p[1]*h[0]     +p[1]*h[2]     +p[2]*h[0]     -p[2]*h[1]     )/z);
    k[1]=((g[0]*p[1]     -g[0]*p[2]     -g[1]*p[0]     +g[1]*p[2]     +g[2]*p[0]     -g[2]*p[1]     )/z);
    k[2]=((g[0]*h[1]*p[2]-g[0]*h[2]*p[1]-g[1]*h[0]*p[2]+g[1]*h[2]*p[0]+g[2]*h[0]*p[1]-g[2]*h[1]*p[0])/z);
    k[3]=((q[0]*h[1]     -q[0]*h[2]     -q[1]*h[0]     +q[1]*h[2]     +q[2]*h[0]     -q[2]*h[1]     )/z);
    k[4]=((g[0]*q[1]     -g[0]*q[2]     -g[1]*q[0]     +g[1]*q[2]     +g[2]*q[0]     -g[2]*q[1]     )/z);
    k[5]=((g[0]*h[1]*q[2]-g[0]*h[2]*q[1]-g[1]*h[0]*q[2]+g[1]*h[2]*q[0]+g[2]*h[0]*q[1]-g[2]*h[1]*q[0])/z);

double w= p[0]*q[1]     -p[0]*q[2]     -p[1]*q[0]     +p[1]*q[2]     +p[2]*q[0]     -p[2]*q[1];
        if(w==0) return(false);
        r[0]=((g[0]*q[1]     -g[0]*q[2]     -g[1]*q[0]     +g[1]*q[2]     +g[2]*q[0]     -g[2]*q[1]     )/w);
        r[1]=((p[0]*g[1]     -p[0]*g[2]     -p[1]*g[0]     +p[1]*g[2]     +p[2]*g[0]     -p[2]*g[1]     )/w);
    r[2]=((p[0]*q[1]*g[2]-p[0]*q[2]*g[1]-p[1]*q[0]*g[2]+p[1]*q[2]*g[0]+p[2]*q[0]*g[1]-p[2]*q[1]*g[0])/w);
        r[3]=((h[0]*q[1]     -h[0]*q[2]     -h[1]*q[0]     +h[1]*q[2]     +h[2]*q[0]     -h[2]*q[1]     )/w);
        r[4]=((p[0]*h[1]     -p[0]*h[2]     -p[1]*h[0]     +p[1]*h[2]     +p[2]*h[0]     -p[2]*h[1]     )/w);
    r[5]=((p[0]*q[1]*h[2]-p[0]*q[2]*h[1]-p[1]*q[0]*h[2]+p[1]*q[2]*h[0]+p[2]*q[0]*h[1]-p[2]*q[1]*h[0])/w);

        s[0]= k[4]*z/w;
        s[1]=-k[1]*z/w;
//      s[2]=g[0]-s[0]*p[0]-s[1]*q[0];
    s[2]=-k[2]*s[0]-k[5]*s[1];
        s[3]=-k[3]*z/w;
        s[4]= k[0]*z/w;
//      s[5]=h[0]-s[3]*p[0]-s[4]*q[0];
    s[5]=-k[2]*s[3]-k[5]*s[4];
        return(true);
}
*/

void CProLatLon::OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS)
{
    CPoint pt, pt1,pt2;

//    double min_lat, max_lat;
  //  double min_lon, max_lon;
    double dbl, db2, db3;
        CSize Sz=pDoc->pMyMap->GetSize();

    dbl=xy2Lat(    0,     0);
    min_lat=             dbl         ;
    max_lat=             dbl         ;
    dbl=xy2Lon(    0,     0);
    min_lon=             dbl         ;
    max_lon=             dbl         ;

    dbl=xy2Lat(Sz.cx,     0);
    min_lat=(dbl<min_lat?dbl:min_lat);
    max_lat=(dbl>max_lat?dbl:max_lat);
    dbl=xy2Lon(Sz.cx,     0);
    min_lon=(dbl<min_lon?dbl:min_lon);
    max_lon=(dbl>max_lon?dbl:max_lon);

    dbl=xy2Lat(    0, Sz.cy);
    min_lat=(dbl<min_lat?dbl:min_lat);
    max_lat=(dbl>max_lat?dbl:max_lat);
    dbl=xy2Lon(    0, Sz.cy);
    min_lon=(dbl<min_lon?dbl:min_lon);
    max_lon=(dbl>max_lon?dbl:max_lon);

    dbl=xy2Lat(Sz.cx, Sz.cy);
    min_lat=(dbl<min_lat?dbl:min_lat);
    max_lat=(dbl>max_lat?dbl:max_lat);
    dbl=xy2Lon(Sz.cx, Sz.cy);
    min_lon=(dbl<min_lon?dbl:min_lon);
    max_lon=(dbl>max_lon?dbl:max_lon);

    min_lat=(-90>min_lat?-90:min_lat);
    max_lat=( 90<max_lat? 90:max_lat);

    dbl=log(max_lat-min_lat)/log(10);
        db2=1/60.0;
    if(dbl>-1.25){db2=1/30.0;}
    if(dbl>-1.00){db2=1/20.0;}
    if(dbl>-0.75){db2=1/15.0;}
    if(dbl>-0.50){db2=1/12.0;}
    if(dbl>-0.25){db2=1/10.0;}
    if(dbl> 0.00){db2=1/ 6.0;}
    if(dbl> 0.25){db2=1/ 4.0;}
    if(dbl> 0.45){db2=1/ 3.0;}
    if(dbl> 0.65){db2=1/ 2.0;}
    if(dbl> 0.85){db2= 1;}
    if(dbl> 1.05){db2= 2;}
    if(dbl> 1.25){db2= 4;}
    if(dbl> 1.45){db2= 5;}
    if(dbl> 1.65){db2=10;}
    if(dbl> 1.85){db2=15;}
    if(dbl> 2.05){db2=20;}
    if(dbl> 2.25){db2=30;}

        pDC->SelectObject(pDoc->pen1);

    for(db3=db2*(int)(min_lat/db2)+(min_lat<0?0:db2);db3<max_lat;db3+=db2)
    {
        pt1=LL2xy(db3,min_lon);
        pt2=LL2xy(db3,max_lon);
                pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
                pDC->MoveTo(pt.x, pt.y);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
    }

    for(db3=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db3<max_lon;db3+=db2)
    {
        pt1=LL2xy(min_lat,db3);
        pt2=LL2xy(max_lat,db3);
                pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
                pDC->MoveTo(pt.x, pt.y);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
    }

        pDC->SelectObject(pDoc->pen0);

// equator:
    pt1=LL2xy(  0,min_lon);
    pt2=LL2xy(  0,max_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// 90 S:
    pt1=LL2xy(-90,min_lon);
    pt2=LL2xy(-90,max_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// 90 N:
    pt1=LL2xy( 90,min_lon);
    pt2=LL2xy( 90,max_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// Greenwich:
        pDC->SelectObject(pDoc->pen0);

    pt1=LL2xy(-90, 360);
    pt2=LL2xy( 90, 360);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

    pt1=LL2xy(-90,   0);
    pt2=LL2xy( 90,   0);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

    pt1=LL2xy(-90,-360);
    pt2=LL2xy( 90,-360);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// 180 E == 180 W:
    pt1=LL2xy(-90, 180);
    pt2=LL2xy( 90, 180);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

// 180 W == 180 E:
    pt1=LL2xy(-90,-180);
    pt2=LL2xy( 90,-180);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

        return;
}
