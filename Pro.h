// Pro.h: interface for the CPro class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRO_H__9A1BF540_405D_11D9_9098_8614D0E27160__INCLUDED_)
#define AFX_PRO_H__9A1BF540_405D_11D9_9098_8614D0E27160__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define unknown         0
#define conformal       1
#define equidistant     2
#define equalarea       3

//#define unknown       0
#define azimuthal       4
#define cilindric       8
#define conic           12
#define pseudoconic     16
#define polyconic       20
//#define .........     24
//#define .........     28

//#define unknown       0
#define normal          32
#define transverse      64
#define oblique         96

//-----------------
//#define unknown               0

// Latitude/Longitude // Ozi ???
#define latlon          equidistant     +cilindric      +normal         +1*128

// Mercator // Ozi
#define merc            conformal       +cilindric      +normal         +1*128

// Transverse Mercator // Ozi
#define tmerc           conformal       +cilindric      +transverse     +1*128

// Universal Transverse Mercator // Ozi
#define utm             conformal       +cilindric      +transverse     +2*128

// Gauss-Krueger // ???
#define gk              conformal       +cilindric      +transverse     +3*128

// Azimuthal Equidistant // ???
#define azeqd           equidistant     +azimuthal   /* +normal */      +1*128

// Lambert Conformal Conic // Ozi
#define lcc             conformal       +conic          +0              +1*128

// Albers Equal Area // Ozi
#define aea             equalarea       +0              +0              +1*128

// Polyconic (American) // Ozi
#define poly            0               +polyconic      +0              +1*128

// Sinusoidal // Ozi
#define sinu            equidistant     +0              +0              +1*128

// (SUI) Swiss Grid // Ozi
#define somerc          0               +0              +0              +2*128

// Vertical Near-Sided Perspective // Ozi
#define nsper           0               +0              +0              +3*128

#include "GeoPoint.h"
#include "CalPoint.h"
//class CGeoPoint;
//class CCalPoint;
#include "MyMap.h"

double RAD(double DEG);
double DEG(double RAD);
double MyPoly(int i, double x, double y);

class CPro  
{
public:
        int ProType;

protected:
    CMapomDoc *pDoc;
//    double k[6];
//    double r[6];
    double k[MaxCalibrationPoints*2];
    double r[MaxCalibrationPoints*2];

    double min_lat, max_lat;
    double min_lon, max_lon;
    void CPro::Marg();
	int N; // CalPoints used

public:
    CPro();
    CPro(CMapomDoc* pDoc);
    virtual ~CPro();
    double xy2Lat(CPoint P);
    double xy2Lon(CPoint P);
    CGeoPoint xy2LL(CPoint P);
    CGeoPoint xy2LL(int x, int y);
    int    LL2x  (double lat, double lon);
    int    LL2y  (double lat, double lon);
    CPoint LL2xy (double lat, double lon);
    CPoint LL2xy (CGeoPoint GP);

//        virtual double CPro::Lat2z(double Lat);
  //      virtual double CPro::z2Lat(double z  );
    //    virtual double CPro::Lon2z(double Lon);
      //  virtual double CPro::z2Lon(double z  );

    virtual double CPro::LL2w(CGeoPoint GP);
    virtual double CPro::LL2z(CGeoPoint GP);
    virtual double CPro::wz2Lat(double w, double z);
    virtual double CPro::wz2Lon(double w, double z);

    virtual int    LL2x  (CGeoPoint GP);
    virtual int    LL2y  (CGeoPoint GP);
    virtual double xy2Lat(int x, int y);
    virtual double xy2Lon(int x, int y);
    bool Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated);
    bool Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3);
    bool Calibrate();
    virtual void OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS);
};

#endif // !defined(AFX_PRO_H__9A1BF540_405D_11D9_9098_8614D0E27160__INCLUDED_)
