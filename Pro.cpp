// Pro.cpp: implementation of the CPro class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "Pro.h"
#include "GeoPoint.h"

#include "mapomDoc.h"
#include "mapoView.h"
#include "math.h"

#include "matrix.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

double RAD(double DEG)
{return(DEG*3.141592653589793/180);
}

double DEG(double RAD)
{return(RAD*180/3.141592653589793);
}

void CPro::Marg() // find @ map: min_lat, max_lat, min_lon, max_lon
{
CGeoPoint GP0;
CPoint P_min_lat, P_max_lat, P_min_lon, P_max_lon, P0;

        CSize Sz=pDoc->pMyMap->GetSize();

GP0=xy2LL(    0,     0);
 P0=LL2xy(GP0);
 if(P0.x<0)P0.x=0;
 if(P0.y<0)P0.x=0;
 if(P0.x>Sz.cx)P0.x=Sz.cx;
 if(P0.y>Sz.cy)P0.y=Sz.cy;
P_min_lat=P_max_lat=P_min_lon=P_max_lon=P0;

GP0=xy2LL(Sz.cx, Sz.cx);
 P0=LL2xy(GP0);
 if(P0.x<0)P0.x=0;
 if(P0.y<0)P0.x=0;
 if(P0.x>Sz.cx)P0.x=Sz.cx;
 if(P0.y>Sz.cy)P0.y=Sz.cy;

if(xy2Lat(P_min_lat)>xy2Lat(P0))P_min_lat=P0;
if(xy2Lat(P_max_lat)<xy2Lat(P0))P_max_lat=P0;
if(xy2Lon(P_min_lon)>xy2Lon(P0))P_min_lon=P0;
if(xy2Lon(P_max_lon)<xy2Lon(P0))P_max_lon=P0;

GP0=xy2LL(    0, Sz.cx);
 P0=LL2xy(GP0);
 if(P0.x<0)P0.x=0;
 if(P0.y<0)P0.x=0;
 if(P0.x>Sz.cx)P0.x=Sz.cx;
 if(P0.y>Sz.cy)P0.y=Sz.cy;

if(xy2Lat(P_min_lat)>xy2Lat(P0))P_min_lat=P0;
if(xy2Lat(P_max_lat)<xy2Lat(P0))P_max_lat=P0;
if(xy2Lon(P_min_lon)>xy2Lon(P0))P_min_lon=P0;
if(xy2Lon(P_max_lon)<xy2Lon(P0))P_max_lon=P0;

GP0=xy2LL(Sz.cx,     0);
 P0=LL2xy(GP0);
 if(P0.x<0)P0.x=0;
 if(P0.y<0)P0.x=0;
 if(P0.x>Sz.cx)P0.x=Sz.cx;
 if(P0.y>Sz.cy)P0.y=Sz.cy;

if(xy2Lat(P_min_lat)>xy2Lat(P0))P_min_lat=P0;
if(xy2Lat(P_max_lat)<xy2Lat(P0))P_max_lat=P0;
if(xy2Lon(P_min_lon)>xy2Lon(P0))P_min_lon=P0;
if(xy2Lon(P_max_lon)<xy2Lon(P0))P_max_lon=P0;

//search for real min_lat
for(;1;P_min_lat.x=P0.x,P_min_lat.y=P0.y)
{
P0.x=P_min_lat.x+0,P0.y=P_min_lat.y+1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lat(P0)<xy2Lat(P_min_lat))continue;
P0.x=P_min_lat.x+0,P0.y=P_min_lat.y-1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lat(P0)<xy2Lat(P_min_lat))continue;
P0.x=P_min_lat.x+1,P0.y=P_min_lat.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lat(P0)<xy2Lat(P_min_lat))continue;
P0.x=P_min_lat.x-1,P0.y=P_min_lat.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lat(P0)<xy2Lat(P_min_lat))continue;
P0.x=P_min_lat.x+0,P0.y=P_min_lat.y+0;break;
}
min_lat=xy2Lat(P_min_lat);

//search for real max_lat
for(;1;P_max_lat.x=P0.x,P_max_lat.y=P0.y)
{
P0.x=P_max_lat.x+0,P0.y=P_max_lat.y+1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lat(P0)>xy2Lat(P_max_lat))continue;
P0.x=P_max_lat.x+0,P0.y=P_max_lat.y-1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lat(P0)>xy2Lat(P_max_lat))continue;
P0.x=P_max_lat.x+1,P0.y=P_max_lat.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lat(P0)>xy2Lat(P_max_lat))continue;
P0.x=P_max_lat.x-1,P0.y=P_max_lat.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lat(P0)>xy2Lat(P_max_lat))continue;
P0.x=P_max_lat.x+0,P0.y=P_max_lat.y+0;break;
}
max_lat=xy2Lat(P_max_lat);

//search for real min_lon
for(;1;P_min_lon.x=P0.x,P_min_lon.y=P0.y)
{
P0.x=P_min_lon.x+0,P0.y=P_min_lon.y+1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)<xy2Lon(P_min_lon))continue;
P0.x=P_min_lon.x+0,P0.y=P_min_lon.y-1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)<xy2Lon(P_min_lon))continue;
P0.x=P_min_lon.x+1,P0.y=P_min_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)<xy2Lon(P_min_lon))continue;
P0.x=P_min_lon.x-1,P0.y=P_min_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)<xy2Lon(P_min_lon))continue;

min_lon=xy2Lon(P_min_lon);
P0.x=P_min_lon.x+0,P0.y=P_min_lon.y+1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)*xy2Lon(P_min_lon)<0)min_lon=-180;
P0.x=P_min_lon.x+0,P0.y=P_min_lon.y-1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)*xy2Lon(P_min_lon)<0)min_lon=-180;
P0.x=P_min_lon.x+1,P0.y=P_min_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)*xy2Lon(P_min_lon)<0)min_lon=-180;
P0.x=P_min_lon.x-1,P0.y=P_min_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)*xy2Lon(P_min_lon)<0)min_lon=-180;
break;
}

//search for real max_lon
for(;1;P_max_lon.x=P0.x,P_max_lon.y=P0.y)
{
P0.x=P_max_lon.x+0,P0.y=P_max_lon.y+1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)>xy2Lon(P_max_lon))continue;
P0.x=P_max_lon.x+0,P0.y=P_max_lon.y-1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)>xy2Lon(P_max_lon))continue;
P0.x=P_max_lon.x+1,P0.y=P_max_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)>xy2Lon(P_max_lon))continue;
P0.x=P_max_lon.x-1,P0.y=P_max_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)>xy2Lon(P_max_lon))continue;

max_lon=xy2Lon(P_max_lon);
P0.x=P_max_lon.x+0,P0.y=P_max_lon.y+1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)*xy2Lon(P_max_lon)<0)max_lon= 180;
P0.x=P_max_lon.x+0,P0.y=P_max_lon.y-1;
if(0<=P0.y&&P0.y<=Sz.cy&&xy2Lon(P0)*xy2Lon(P_max_lon)<0)max_lon= 180;
P0.x=P_max_lon.x+1,P0.y=P_max_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)*xy2Lon(P_max_lon)<0)max_lon= 180;
P0.x=P_max_lon.x-1,P0.y=P_max_lon.y+0;
if(0<=P0.x&&P0.x<=Sz.cx&&xy2Lon(P0)*xy2Lon(P_max_lon)<0)max_lon= 180;
break;
}

return;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPro::CPro()
{
    ProType=unknown;
    for(int i=0;i<MaxCalibrationPoints*2;i++)
    {
        k[i]=r[0]=0;
    }
    min_lat=0;
    max_lat=0;
    min_lon=0;
    max_lon=0;
//    min_lat=  -90;
//    max_lat=   90;
//    min_lon= -180;
//    max_lon=  180;
}

CPro::CPro(CMapomDoc* pDoc)
{
    ProType=unknown;
    for(int i=0;i<MaxCalibrationPoints*2;i++)
    {
        k[i]=r[0]=0;
    }
    min_lat=0;
    max_lat=0;
    min_lon=0;
    max_lon=0;
    this->pDoc=pDoc;
}

CPro::~CPro()
{

}

double CPro::xy2Lat(CPoint P)
{return(xy2Lat(P.x, P.y));
}

double CPro::xy2Lon(CPoint P)
{return(xy2Lon(P.x, P.y));
}

CPoint CPro::LL2xy(double lat, double lon)
{return(LL2xy(CGeoPoint(lat, lon)));
}

CPoint CPro::LL2xy(CGeoPoint GP)
{return(CPoint(LL2x(GP), LL2y(GP)));
}

int CPro::LL2x(double lat, double lon)
{return(LL2x(CGeoPoint(lat, lon)));
}

int CPro::LL2y(double lat, double lon)
{return(LL2y(CGeoPoint(lat, lon)));
}

CGeoPoint CPro::xy2LL(CPoint P)
{return(CGeoPoint(xy2Lat(P.x, P.y), xy2Lon(P.x, P.y)));
}

CGeoPoint CPro::xy2LL(int x, int y)
{return(CGeoPoint(xy2Lat(x, y), xy2Lon(x, y)));
}

// ==== virtuals ====

/*
double CPro::Lat2z(double Lat)
{return(Lat);
}

double CPro::z2Lat(double z)
{return(z);
}

double CPro::Lon2z(double Lon)
{return(Lon);
}

double CPro::z2Lon(double z)
{return(z);
}
*/

double CPro::LL2w(CGeoPoint GP)
{return(GP.GetLon());
}

double CPro::LL2z(CGeoPoint GP)
{return(GP.GetLat());
}

double CPro::wz2Lat(double w, double z)
{//	return(z);
	CGeoPoint GP=CGeoPoint(z,0);
	return(GP.GetLat());
}

double CPro::wz2Lon(double w, double z)
{//	return(w);
	CGeoPoint GP=CGeoPoint(0,w);
	return(GP.GetLon());
}

// ==================

int CPro::LL2x(CGeoPoint GP)
{return(0);
}

int CPro::LL2y(CGeoPoint GP)
{return(0);
}

double CPro::xy2Lat(int x, int y)
{return(0);
}

double CPro::xy2Lon(int x, int y)
{return(0);
}

/*
bool CPro::Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated)
{
        return(false);
}

bool CPro::Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3)
{
        return(false);
}
*/

bool CPro::Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated)
{
	N=2;
        double g[3], h[3]; // x,y
        double p[3], q[3]; // long,lat
        double D, d, D0, D1, D2, D3;

        double s[6];

        g[0]=CP1.GetPoint().x;
        h[0]=CP1.GetPoint().y;
//      q[0]=CP1.GetLat  ()  ;
        q[0]=LL2z(CP1);
//      p[0]=CP1.GetLon  ()  ;
        p[0]=LL2w(CP1);

        g[1]=CP2.GetPoint().x;
        h[1]=CP2.GetPoint().y;
//      q[1]=CP2.GetLat  ()  ;
        q[1]=LL2z(CP2);
//      p[1]=CP2.GetLon  ()  ;
        p[1]=LL2w(CP2);

        double dg=g[1]-g[0];
        double dh=h[1]-h[0];
        double dp=p[1]-p[0];
        double dq=q[1]-q[0];
        double dd=g[0]*h[1]-g[1]*h[0];

        if(rotated)
        {
            D =                  -dg*dg                   -dh*dh;
            if(D==0) return(false);
            D0=                  -dp*dg                   -dq*dh;
            D1=                   dq*dg                   -dp*dh;
            D2=(p[1]*g[0]-p[0]*g[1])*dg+(p[1]*h[0]-p[0]*h[1])*dh+dd*dq;
            D3=(q[1]*g[0]-q[0]*g[1])*dg+(q[1]*h[0]-q[0]*h[1])*dh-dd*dp;

            k[0]=(D0/D);
            k[1]=(D1/D);
            k[2]=(D2/D);
            k[3]=-k[1];
            k[4]= k[0];
            k[5]=(D3/D);
        }
        else
        { // normal-oriented
            D =-dg*dh;
            if(D==0) return(false);
            D0=                  -dp*dh;
            D1=(p[1]*g[0]-p[0]*g[1])*dh;
            D2=                  -dq*dg;
            D3=(q[1]*h[0]-q[0]*h[1])*dg;

            k[0]=(D0/D);
            k[1]=0;
            k[2]=(D1/D);
            k[3]=0;
            k[4]=(D2/D);
            k[5]=(D3/D);
        }

        // ====

//      g[0]=CP1.GetLat  ()  ;
        g[0]=LL2z(CP1);
//      h[0]=CP1.GetLon  ()  ;
        h[0]=LL2w(CP1);
        p[0]=CP1.GetPoint().x;
        q[0]=CP1.GetPoint().y;

//      g[1]=CP2.GetLat  ()  ;
        g[1]=LL2z(CP2);
//      h[1]=CP2.GetLon  ()  ;
        h[1]=LL2w(CP2);
        p[1]=CP2.GetPoint().x;
        q[1]=CP2.GetPoint().y;

        dg=g[1]-g[0];
        dh=h[1]-h[0];
        dp=p[1]-p[0];
        dq=q[1]-q[0];
        dd=g[0]*h[1]-g[1]*h[0];

        if(rotated)
        {

            d =                  -dg*dg                   -dh*dh;
            if(d==0) return(false);
            D0=                  -dp*dg                   -dq*dh;
            D1=                   dq*dg                   -dp*dh;
            D2=(p[1]*g[0]-p[0]*g[1])*dg+(p[1]*h[0]-p[0]*h[1])*dh+dd*dq;
            D3=(q[1]*g[0]-q[0]*g[1])*dg+(q[1]*h[0]-q[0]*h[1])*dh-dd*dp;

            r[0]=(D0/d);
            r[1]=(D1/d);
            r[2]=(D2/d);
            r[3]=-r[1];
            r[4]= r[0];
            r[5]=(D3/d);

            s[0]=( k[0]*D/d);
            s[1]=(-k[1]*D/d);
    //      s[2]=;
            s[3]=(-s[1]    );
            s[4]=( s[0]    );
    //      s[5]=;
        }
        else
        { // normal-oriented
            d =-dg*dh;
            if(d==0) return(false);
            D0=                  -dp*dh;
            D1=(p[1]*g[0]-p[0]*g[1])*dh;
            D2=                  -dq*dg;
            D3=(q[1]*h[0]-q[0]*h[1])*dg;

            r[0]=(D0/d);
            r[1]=0;
            r[2]=(D1/d);
            r[3]=0;
            r[4]=(D2/d);
            r[5]=(D3/d);

            s[0]=( k[4]*D/d );
            s[1]= 0;
            //s[2]=g[0]-s[0]*p[0];
            s[2]=(-k[2]*s[0]);
            s[3]= 0;
            s[4]=( k[0]*D/d );
            //s[5]=h[0]-s[4]*q[0];
            s[5]=(-k[5]*s[4]);

        }
        Marg();
        return(true);
}

bool CPro::Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3)
{
	N=3;
	for(int i=0;i<2*MaxCalibrationPoints;i++)
	{
		k[i]=r[i]=0;
	}
        double g[3], h[3]; // x,y
        double p[3], q[3]; // long,lat

        double s[6];

        g[0]=CP1.GetPoint().x;
        h[0]=CP1.GetPoint().y;
//      q[0]=CP1.GetLat  ()  ;
        q[0]=LL2z(CP1);
//      p[0]=CP1.GetLon  ()  ;
        p[0]=LL2w(CP1);

        g[1]=CP2.GetPoint().x;
        h[1]=CP2.GetPoint().y;
//      q[1]=CP2.GetLat  ()  ;
        q[1]=LL2z(CP2);
//      p[1]=CP2.GetLon  ()  ;
        p[1]=LL2w(CP2);

        g[2]=CP3.GetPoint().x;
        h[2]=CP3.GetPoint().y;
//      q[2]=CP3.GetLat  ()  ;
        q[2]=LL2z(CP3);
//      p[2]=CP3.GetLon  ()  ;
        p[2]=LL2w(CP3);

double z= g[0]*h[1]     -g[0]*h[2]     -g[1]*h[0]     +g[1]*h[2]     +g[2]*h[0]     -g[2]*h[1];
        if(z==0) return(false);
    k[0]=((p[0]*h[1]     -p[0]*h[2]     -p[1]*h[0]     +p[1]*h[2]     +p[2]*h[0]     -p[2]*h[1]     )/z);
    k[1]=((g[0]*p[1]     -g[0]*p[2]     -g[1]*p[0]     +g[1]*p[2]     +g[2]*p[0]     -g[2]*p[1]     )/z);
    k[2]=((g[0]*h[1]*p[2]-g[0]*h[2]*p[1]-g[1]*h[0]*p[2]+g[1]*h[2]*p[0]+g[2]*h[0]*p[1]-g[2]*h[1]*p[0])/z);
    k[3]=((q[0]*h[1]     -q[0]*h[2]     -q[1]*h[0]     +q[1]*h[2]     +q[2]*h[0]     -q[2]*h[1]     )/z);
    k[4]=((g[0]*q[1]     -g[0]*q[2]     -g[1]*q[0]     +g[1]*q[2]     +g[2]*q[0]     -g[2]*q[1]     )/z);
    k[5]=((g[0]*h[1]*q[2]-g[0]*h[2]*q[1]-g[1]*h[0]*q[2]+g[1]*h[2]*q[0]+g[2]*h[0]*q[1]-g[2]*h[1]*q[0])/z);

double w= p[0]*q[1]     -p[0]*q[2]     -p[1]*q[0]     +p[1]*q[2]     +p[2]*q[0]     -p[2]*q[1];
    if(w==0) return(false);
    r[0]=((g[0]*q[1]     -g[0]*q[2]     -g[1]*q[0]     +g[1]*q[2]     +g[2]*q[0]     -g[2]*q[1]     )/w);
    r[1]=((p[0]*g[1]     -p[0]*g[2]     -p[1]*g[0]     +p[1]*g[2]     +p[2]*g[0]     -p[2]*g[1]     )/w);
    r[2]=((p[0]*q[1]*g[2]-p[0]*q[2]*g[1]-p[1]*q[0]*g[2]+p[1]*q[2]*g[0]+p[2]*q[0]*g[1]-p[2]*q[1]*g[0])/w);
    r[3]=((h[0]*q[1]     -h[0]*q[2]     -h[1]*q[0]     +h[1]*q[2]     +h[2]*q[0]     -h[2]*q[1]     )/w);
    r[4]=((p[0]*h[1]     -p[0]*h[2]     -p[1]*h[0]     +p[1]*h[2]     +p[2]*h[0]     -p[2]*h[1]     )/w);
    r[5]=((p[0]*q[1]*h[2]-p[0]*q[2]*h[1]-p[1]*q[0]*h[2]+p[1]*q[2]*h[0]+p[2]*q[0]*h[1]-p[2]*q[1]*h[0])/w);

    s[0]= k[4]*z/w;
    s[1]=-k[1]*z/w;
//  s[2]= g[0]-s[0]*p[0]-s[1]*q[0];
    s[2]=-k[2]*s[0]-k[5]*s[1];
    s[3]=-k[3]*z/w;
    s[4]= k[0]*z/w;
//  s[5]= h[0]-s[3]*p[0]-s[4]*q[0];
    s[5]=-k[2]*s[3]-k[5]*s[4];
    Marg();
    return(true);
}

double MyPoly(int i, double x, double y)
{
//	int r [MaxCalibrationPoints]={0,1,1,2,2,2,3,3,3,3,4,4,4,4,4,5,5,5,5,5,5,6,6,6,6,6,6,6,7,7};
//	int s [MaxCalibrationPoints]={0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,4,5,6,0,1};
//	t[i]=r[i]-s[i];

//	int px[MaxCalibrationPoints]={0,1,0,2,1,0,3,2,1,0,4,3,2,1,0,5,4,3,2,1,0,6,5,4,3,2,1,0,7,6};
//	int py[MaxCalibrationPoints]={0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,4,5,6,0,1};

	if(i==0)return(x);
	if(i==1)return(y);
	if(i==2)return(1);
	int k;
	k=(int)((pow(i+i+1.0/4,1/2.0)+pow(i+i+9.0/4,1/2.0)-1)/2.0);
	int py= i-k*(k+1)/2;
	int px=-i+k*(k+3)/2; // px=k-py
	return(pow(x,px)*pow(y,py));
}

bool CPro::Calibrate()
{
	int iPoint;
	int rr=0;
	int i,j;
	CCalPoint CalPoint;
	for(iPoint=0;iPoint<MaxCalibrationPoints;iPoint++)
	{
		if(pDoc->pMyMap->GetCalibrationPoint(iPoint).Use())
		{
			rr++;
		}
//		CalPoint=pDoc->pMyMap->GetCalibrationPoint(iPoint);
		k[iPoint]=k[iPoint+MaxCalibrationPoints]=0;
		r[iPoint]=r[iPoint+MaxCalibrationPoints]=0;
	}
N=rr;
	CMatrix Matr1=CMatrix(2*rr,2*rr);
	CVector Vect1=CVector(2*rr);
	CMatrix Matr2=CMatrix(2*rr,2*rr);
	CVector Vect2=CVector(2*rr);

	iPoint=0;
	for(i=0;i<rr;i++)
	{
		iPoint++;
		for(CalPoint=pDoc->pMyMap->GetCalibrationPoint(iPoint);!CalPoint.Use()&&iPoint<=MaxCalibrationPoints;iPoint++)
		{
			if(iPoint>MaxCalibrationPoints)
			{
				return(false); // unknown error
			}
			CalPoint=pDoc->pMyMap->GetCalibrationPoint(iPoint);
		}
//		Vect1[i   ]=CalPoint.GetLon();
//		Vect1[i+rr]=CalPoint.GetLat();
		Vect1[i   ]=LL2w(CalPoint);
		Vect1[i+rr]=LL2z(CalPoint);
		Vect2[i   ]=CalPoint.GetPoint().x;
		Vect2[i+rr]=CalPoint.GetPoint().y;

		for(j=0;j<rr;j++)
		{
//			Matr1[i][j]=0;
			Matr1[i   ][j   ]=Matr1[i+rr][j+rr]=MyPoly(j,      CalPoint.GetPoint().x,      CalPoint.GetPoint().y);
			Matr1[i   ][j+rr]=Matr1[i+rr][j   ]=0;
			Matr2[i   ][j   ]=Matr2[i+rr][j+rr]=MyPoly(j, LL2w(CalPoint)            , LL2z(CalPoint)            );
			Matr2[i   ][j+rr]=Matr2[i+rr][j   ]=0;

		}
	}
	double det1=Matr1.determinant();
	double det2=Matr2.determinant();
    if(det1==0){return(false);}
    if(det2==0){return(false);}

	CMatrix MatrK;
	CMatrix MatrR;
	double detK;
	double detR;
	for(int kk=0;kk<2*rr;kk++)
	{
		MatrK=CMatrix(Matr1);
		MatrR=CMatrix(Matr2);
		for(i=0;i<2*rr;i++)
		{
			MatrK[i][kk]=Vect1[i];
			MatrR[i][kk]=Vect2[i];
		}
		detK=MatrK.determinant();
		detR=MatrR.determinant();
		k[kk]=detK/det1;
		r[kk]=detR/det2;
	}

    Marg();
    return(true);
}

void CPro::OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS)
{
    CPoint pt, pt1,pt2;

//    double min_lat, max_lat;
//    double min_lon, max_lon;
    double dbl, db2, db3, db4;
        CSize Sz=pDoc->pMyMap->GetSize();

//    dbl=xy2Lat(    0,     0);
//    min_lat=             dbl         ;
//    max_lat=              90         ;
//    min_lon=            -180         ;
//    max_lon=             180         ;

//    dbl=xy2Lat(Sz.cx,     0);
//    min_lat=(dbl<min_lat?dbl:min_lat);

//    dbl=xy2Lat(    0, Sz.cy);
//    min_lat=(dbl<min_lat?dbl:min_lat);

//    dbl=xy2Lat(Sz.cx, Sz.cy);
//    min_lat=(dbl<min_lat?dbl:min_lat);

//    min_lat=(-90>min_lat?-90:min_lat);
//    max_lat=( 90<max_lat? 90:max_lat);

    dbl=log(max_lat-min_lat)/log(10);
        db2=1/60.0;
    if(dbl>-1.25){db2=1/30.0;}
    if(dbl>-1.00){db2=1/20.0;}
    if(dbl>-0.75){db2=1/15.0;}
    if(dbl>-0.50){db2=1/12.0;}
    if(dbl>-0.25){db2=1/10.0;}
    if(dbl> 0.00){db2=1/ 6.0;}
    if(dbl> 0.25){db2=1/ 4.0;}
    if(dbl> 0.45){db2=1/ 3.0;}
    if(dbl> 0.65){db2=1/ 2.0;}
    if(dbl> 0.85){db2= 1;}
    if(dbl> 1.05){db2= 2;}
    if(dbl> 1.25){db2= 4;}
    if(dbl> 1.45){db2= 5;}
    if(dbl> 1.65){db2=10;}
    if(dbl> 1.85){db2=15;}
    if(dbl> 2.05){db2=20;}
    if(dbl> 2.25){db2=30;}

        pDC->SelectObject(pDoc->pen1);

    for(db3=db2*(int)(min_lat/db2)+(min_lat<0?0:db2);db3<max_lat;db3+=db2)
    {
        pt1=LL2xy(db3,min_lon);
                pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
                pDC->MoveTo(pt.x, pt.y);

      for(db4=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db4<=max_lon;db4+=db2)
      {
        pt2=LL2xy(db3,db4);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
      }
    }

    for(db3=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db3<=max_lon;db3+=db2)
    {
        pt1=LL2xy(min_lat,db3);
                pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
                pDC->MoveTo(pt.x, pt.y);

      for(db4=db2*(int)(min_lat/db2)+(min_lat<0?0:db2);db4<=max_lat;db4+=db2)
      {
        pt2=LL2xy(db4,db3);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
      }
//	  if(db4<max_lat)
//	  {
		db4=max_lat;
        pt2=LL2xy(db4,db3);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
//	  }
    }

        pDC->SelectObject(pDoc->pen0);

// equator:
/*    pt1=LL2xy(  0,min_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);

      for(double db4=db2*(int)(min_lon/db2)+(min_lon<0?0:db2);db4<=max_lon;db4+=db2)
      {
        pt2=LL2xy(db3,db4);
                pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
                pDC->LineTo(pt.x, pt.y);
      }*/

// 90 S:
/*    pt1=LL2xy(-90,min_lon);
    pt2=LL2xy(-90,max_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);*/

// 90 N:
/*    pt1=LL2xy( 90,min_lon);
    pt2=LL2xy( 90,max_lon);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);*/

// Greenwich:
/*        pDC->SelectObject(pDoc->pen0);

    pt1=LL2xy(-90, 360);
    pt2=LL2xy( 90, 360);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

    pt1=LL2xy(-90,   0);
    pt2=LL2xy( 90,   0);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);

    pt1=LL2xy(-90,-360);
    pt2=LL2xy( 90,-360);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);*/

// 180 E == 180 W:
/*    pt1=LL2xy(-90, 180);
    pt2=LL2xy( 90, 180);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);*/

// 180 W == 180 E:
/*    pt1=LL2xy(-90,-180);
    pt2=LL2xy( 90,-180);
        pt=ClientCS?pDoc->GetView()->page2client(pt1):pt1;
    pDC->MoveTo(pt.x, pt.y);
        pt=ClientCS?pDoc->GetView()->page2client(pt2):pt2;
    pDC->LineTo(pt.x, pt.y);*/

        return;
}
