// NMEA0183.h: interface for the CNMEA0183 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NMEA0183_H__CD1E22C2_71F3_4B5B_9952_635B415D8ABD__INCLUDED_)
#define AFX_NMEA0183_H__CD1E22C2_71F3_4B5B_9952_635B415D8ABD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define GPGGA 1
#define GPGLL 2
#define GPGSA 3
#define GPGSV 4
#define GPZDA 5

class CNMEA0183  
{
public:
	CNMEA0183();
	CNMEA0183(CString);
	virtual ~CNMEA0183();
	int GetType();

protected:
	int CheckSum0;
	int CheckSum1;
	CString Msg;

	double GGA01; //UTC (GMT) ------ hhmmss.ss or ssssss.ss ????????
	double GGA02; //lat
	char   GGA03; //N(orth)/S(outh)
	double GGA04; //lon
	char   GGA05; //E(ast)/W(est)
	int    GGA06; //precision indicator
	int    GGA07; //used sats
	float  GGA08; //HDOP
	int    GGA09; //alt
	char   GGA10; //M(eters)
	float  GGA11; //sea alt (above/below WGS-84)
	char   GGA12; //M(eters)
	float  GGA13; //dif data age
	int    GGA14; //dif data station id

	char   GSA01; //mode M(anual)/A(utomatic)
	char   GSA02; //mode undef/2D/3D
	int    GSA03; //used sat N
	int    GSA04; //used sat N
	int    GSA05; //used sat N
	int    GSA06; //used sat N
	int    GSA07; //used sat N
	int    GSA08; //used sat N
	int    GSA09; //used sat N
	int    GSA10; //used sat N
	int    GSA11; //used sat N
	int    GSA12; //used sat N
	int    GSA13; //used sat N
	int    GSA14; //used sat N
	float  GSA15; //PDOP
	float  GSA16; //HDOP
	float  GSA17; //VDOP

	int    GSV01; //GSV messages
	int    GSV02; //GSV message N
	int    GSV03; //sats observed
	int    GSV04; //(1) sat PRN
	int    GSV05; //(1) vertical deg
	int    GSV06; //(1) asimut
	int    GSV07; //(1) SNR, dB
	int    GSV08; //(2) sat PRN
	int    GSV09; //(2) vertical deg
	int    GSV10; //(2) asimut
	int    GSV11; //(2) SNR, dB
	int    GSV12; //(3) sat PRN
	int    GSV13; //(3) vertical deg
	int    GSV14; //(3) asimut
	int    GSV15; //(3) SNR, dB
	int    GSV16; //(4) sat PRN
	int    GSV17; //(4) vertical deg
	int    GSV18; //(4) asimut
	int    GSV19; //(4) SNR, dB

	double ZDA01; //UTC (GMT) ------ hhmmss.ss or ssssss.ss ????????
	int    ZDA02; //day of month
	int    ZDA03; //month
	int    ZDA04; //year
	int    ZDA05; //TZ hours (GMT+-)
	int    ZDA06; //TZ minutes
};

#endif // !defined(AFX_NMEA0183_H__CD1E22C2_71F3_4B5B_9952_635B415D8ABD__INCLUDED_)
