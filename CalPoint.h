// CalPoint.h: interface for the CCalPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALPOINT_H__4975BB5B_14EB_4A27_A58A_9350367C5CA2__INCLUDED_)
#define AFX_CALPOINT_H__4975BB5B_14EB_4A27_A58A_9350367C5CA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GeoPoint.h"

class CMapomDoc;

class CCalPoint : public CGeoPoint  
{
	friend class CCalibDlg;
protected:
	CPoint Point;
	bool UseIt;

public:
	CPoint GetPoint();
//	void Copy(CCalPoint CalPoint);
	CCalPoint();
	CCalPoint(CPoint Pt                           );
	CCalPoint(              CGeoPoint GeoPoint    );
	CCalPoint(CPoint Pt   , CGeoPoint GeoPoint    );
	CCalPoint(int x, int y, float lat, float lon);
	virtual ~CCalPoint();
	void operator=(CCalPoint CalPoint);
	void Serialize(CArchive& ar, int iPoint);
    void OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS);
	bool Use();
	void Use(bool setUseIt);
};

#endif // !defined(AFX_CALPOINT_H__4975BB5B_14EB_4A27_A58A_9350367C5CA2__INCLUDED_)
