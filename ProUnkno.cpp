// ProUnkno.cpp: implementation of the CProUnknown class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "ProUnkno.h"
//#include "Pro.h"

#include "mapomDoc.h"
#include "mapoView.h"
#include "math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProUnknown::CProUnknown()
{
        ProType=unknown; // see: Pro.h
}

CProUnknown::CProUnknown(CMapomDoc* pDoc)
{
        ProType=unknown; // see: Pro.h
    this->pDoc=pDoc;
}

CProUnknown::~CProUnknown()
{

}

int CProUnknown::LL2x(CGeoPoint GP)
{
	double x=0;
//	for(int i=0;i<MaxCalibrationPoints;i++)
	for(int i=0;i<       N            ;i++)
	{
		x+=r[i                     ]*MyPoly(i,LL2w(GP),LL2z(GP));
	}
	return((int)x);
}

int CProUnknown::LL2y(CGeoPoint GP)
{
	double y=0;
//	for(int i=0;i<MaxCalibrationPoints;i++)
	for(int i=0;i<       N            ;i++)
	{
		y+=r[i+            N       ]*MyPoly(i,LL2w(GP),LL2z(GP));
	}
	return((int)y);
}

double CProUnknown::xy2Lat(int x, int y)
{
//	if(x==100&&y==200)//debug
//	{
//		int xx=x;
//	}
	double ww=0;
	double zz=0;
//	for(int i=0;i<MaxCalibrationPoints;i++)
	for(int i=0;i<       N            ;i++)
	{
		ww+=k[i                     ]*MyPoly(i,          x,          y);
		zz+=k[i+        N           ]*MyPoly(i,          x,          y);
	}
	return(wz2Lat(ww,zz));
}

double CProUnknown::xy2Lon(int x, int y)
{
	double ww=0;
	double zz=0;
//	for(int i=0;i<MaxCalibrationPoints;i++)
	for(int i=0;i<       N            ;i++)
	{
		ww+=k[i                     ]*MyPoly(i,          x,          y);
		zz+=k[i+        N           ]*MyPoly(i,          x,          y);
	}
	return(wz2Lon(ww,zz));
}

//double CProUnknown::LL2z(CGeoPoint GP) //debug
//{return(log(tan((RAD(GP.GetLat()))/2+3.1415926/4)));
//}

//double CProUnknown::wz2Lat(double w, double z) //debug
//{//return(DEG(2*(atan(exp(z))-3.1415926/4)));
//	CGeoPoint GP=CGeoPoint(DEG(2*(atan(exp(z))-3.1415926/4)),0);
//	return(GP.GetLat());
//}

