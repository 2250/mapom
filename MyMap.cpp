// MyMap.cpp: implementation of the CMyMap class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "MyMap.h"
#include "MapomDoc.h"
#include "MapoView.h"
#include "math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyMap::CMyMap() // fuflo
{
	Loaded=false;
	for(int i=0;i<MaxCalibrationPoints;i++)CalPoint[i]=CCalPoint();
	Title="Deep Space :)";
	MagneticVariation=0;
	Projection="Latitude/Longitude";
	PolyCal     =false;
	AutoCalOnly =false;
	BSBUseWPX   =false;

    pDoc=NULL;
}

CMyMap::CMyMap(CMapomDoc* pDoc)
{
	Loaded=false;
	for(int i=0;i<MaxCalibrationPoints;i++)CalPoint[i]=CCalPoint();
	Title="Deep Space :)";
	MagneticVariation=0;
	Projection="Latitude/Longitude";
	PolyCal     =false;
	AutoCalOnly =false;
	BSBUseWPX   =false;

//	CMyMap();
    this->pDoc=pDoc;
}

CMyMap::~CMyMap()
{
    pDoc=NULL;              // :-)
}

CSize CMyMap::GetSize()
{
	return(Loaded?m_pict.GetImageSize():CSize(800,600)); // 800*600 "ot fonarya"
}

bool CMyMap::LoadPict()
{
    return(Loaded=m_pict.Load((LPCTSTR)PathName)); // sic!
}

bool CMyMap::LoadPict(LPCTSTR pszPathName)
{
	PathName=pszPathName;
    return(Loaded=m_pict.Load(pszPathName)); // sic!
}

void CMyMap::OnDraw(CDC *pDC, int DrawFlags, bool ClientCS)
{
    CPen*pen0=pDoc->pen0;
    CPen*pen1=pDoc->pen1;
    CPoint pt, pt1,pt2;

    CRect rc;

    CSize sz = m_pict.GetImageSize();
//    rc.SetRect(0,0,sz.cx,sz.cy);
    if(DrawFlags&DrawMap && Loaded)
    {

		if(ClientCS)
		{
				rc=CRect(pDoc->GetView()->page2client(CRect(0, 0, sz.cx, sz.cy)));
		}
		else
		{
				rc.SetRect(0,0,0,0);
		}
		m_pict.Render(pDC,rc);
	}
//    double min_lat, max_lat;
//    double min_lon, max_lon;
//    double dbl, db2, db3;
//	CSize Sz=GetSize();
    if(DrawFlags&DrawGrid)
    {
		pDoc->pPro->OnDraw(pDC,pDoc,ClientCS);
    }

    if(DrawFlags&DrawCalPoints)
    {
		for(int i=0;i<MaxCalibrationPoints;i++)
		{	
			if(CalPoint[i].Use())CalPoint[i].OnDraw(pDC,pDoc,ClientCS);
		}
	}

    if(DrawFlags&DrawComments)
    {
		for(int i=0;i<MaxMapComments;i++)
		{
			if(!MapComment[i].isEmpty())MapComment[i].OnDraw(pDC,pDoc,ClientCS);
		}
	}
}

bool CMyMap::SetCalibrationPoint(CCalPoint CalPt, int iPoint) // if iPoint==0 then auto-mode
{
	if(iPoint>0&&iPoint<=MaxCalibrationPoints)
	{
		CalPoint[iPoint-1]=CalPt;
//		use[iPoint-1]=true; //??
		return(true);
	}
	else
	{
		for(int i=0;i<MaxCalibrationPoints;i++)
		{
			if(!CalPoint[i].Use())
			{
				CalPoint[i]=CalPt;
				CalPoint[i].Use(true);
				return(true);
			}
		}
		return(false);
	}
}

CCalPoint CMyMap::GetCalibrationPoint(int iPoint) // [1..MaxCalibrationPoints]
{
	CCalPoint CalPt;
	if(iPoint>0&&iPoint<=MaxCalibrationPoints)CalPt=CalPoint[iPoint-1];
	return(CalPt);
}

void CMyMap::Serialize(CArchive& ar)
{
	CString S;
	CString S1,S2,S3,S12;
	CString S21,S22,S23,S24,S25,S26,S27,S28,S29;
	if (ar.IsStoring())
	{
//		ar <<;
		// Title of Map - any text string
		S.Format("%s",Title);
		ar.WriteString((LPCTSTR)(S+"\r\n"));
//		ar.WriteString((LPCTSTR)(Title   +"\r\n"));

		// Link to map image file
		S.Format("%s",PathName);
		ar.WriteString((LPCTSTR)(S+"\r\n"));
//		ar.WriteString((LPCTSTR)(PathName+"\r\n"));

		// only used for special types of maps
		// Note - cannot be modified by the user but must be there
		S.Format("%d,%s,%s",1,"Map Code","");
		ar.WriteString((LPCTSTR)(S+"\r\n"));
//		ar.WriteString("1 ,Map Code,\r\n");

		// Datum settings
		// Note - for normal maps only the datum in the first field is used
		// the rest of the parameters are for datum shifts
		// and there is normally no need for these to be modified by the user
		ar.WriteString("WGS 84,WGS 84,   0.0000,   0.0000,WGS 84\r\n");

		// Reserved fields, the lines must be there
		ar.WriteString("Reserved 1\r\n");
		ar.WriteString("Reserved 2\r\n");

		// Magnetic Variation entry - degrees,minutes
		S.Format("%s,%02d,%06.3f,%c","Magnetic Variation",
			                            (int)(fabs(MagneticVariation)),
			60*(fabs(MagneticVariation)-(int)(fabs(MagneticVariation))),
			MagneticVariation<0?'W':MagneticVariation>0?'E':' ');
		ar.WriteString((LPCTSTR)(S+"\r\n"));
//		ar.WriteString("Magnetic Variation,,,E\r\n");

		// Map Projection
		// Parameters:
		// 1. Map Projection - must match the projection name used in OziExplorer 
		// 2. PolyCal - the next parameter is Yes if Polynomial calibration is used and No if not 
		// 3. AutoCalOnly - Yes if calibration cannot be adjusted by the user No if it can 
		// 4. BSBUseWPX - For BSB images only, Yes if the calibration equations contained in the BSB file are used. 
		S.Format("%s,%s,%s,%s,%s,%s,%s,%s",
			"Map Projection", Projection,
			"PolyCal"		, PolyCal    ?"Yes":"No",
			"AutoCalOnly"	, AutoCalOnly?"Yes":"No",
			"BSBUseWPX"		, BSBUseWPX  ?"Yes":"No");
		ar.WriteString((LPCTSTR)(S+"\r\n"));
//		ar.WriteString("Map Projection,Lambert Conformal Conic,PolyCal,No,AutoCalOnly,No,BSBUseWPX,Yes\r\n");

		// Calibration points, there are always 30
		// Only 9 points can be modified from within OziExplorer
		// but OziExplorer will use the full 30 if they exist in the .map file
		for(int i=0;i<MaxCalibrationPoints;i++)
		{
			CalPoint[i].Serialize(ar, i+1);
		}

		// Projection setup parmeters, all on one line.
		S.Format("%s,,,,,,,",
			"Projection Setup");
		ar.WriteString((LPCTSTR)(S+"\r\n"));

		// A marker line, it contains no information but is used as a file marker, must be there
		S.Format("%s",
			"Map Feature = MF ; Map Comment = MC     These follow if they exist");
		ar.WriteString((LPCTSTR)(S+"\r\n"));

		for(i=0;i<MaxMapComments;i++)
		{
			if(!MapComment[i].isEmpty())
			{
				S.Format("%s,%d,%11.6f,%11.6f,%d,%d,%d,%d,%d,%d,%d",
					"MC",
					i+1,
					MapComment[i].GetLat(),
					MapComment[i].GetLon(),
					0,
					MapComment[i].GetForeColor(),
					MapComment[i].GetBackColor(),
					MapComment[i].GetWidth(),
					MapComment[i].GetHeight(),
					MapComment[i].GetFontSize(),
					MapComment[i].GetFontWeight()>555?1:0);
				ar.WriteString((LPCTSTR)(S+"\r\n"));
				S.Format("%s",MapComment[i].GetComment());
				ar.WriteString((LPCTSTR)(S+"\r\n"));
			}
		}

		// Attached file marker, .plt, .wpt, .evt, .pnt files can be included
		S.Format("%s",
			"Track File = TF      These follow if they exist");
		ar.WriteString((LPCTSTR)(S+"\r\n"));

//TF,.\Data\NewRoad1.plt
//...
//Moving Map Parameters = MM?    These follow if they exist
//MM0,No
//MMPNUM,4
//MMPXY,1,0,0
//MMPXY,2,2500,0
//MMPXY,3,2500,1395
//MMPXY,4,0,1395
//MMPLL,1,-180.000000,  90.000000
//MMPLL,2, 180.000000,  90.000000
//MMPLL,3, 180.000000, -90.000000
//MMPLL,4,-180.000000, -90.000000
//MM1B,16001.269775
//LL Grid Setup
//LLGRID,Yes,10 Deg,Yes,12632256,12632256,12632256,30 Deg,8421504,16777215,7,1,No,x
//MOP,Map Open Position,0,0

		S.Format("%s,%s,%d,%d",
			"IWH",
			"Map Image Width/Height",
			GetSize().cx,
			GetSize().cy);
		ar.WriteString((LPCTSTR)(S+"\r\n"));
	}
	else
	{
//		ar >>;
		// Title of Map - any text string
		ar.ReadString (Title           );

		// Link to map image file
		ar.ReadString (PathName        );

		// only used for special types of maps
		// Note - cannot be modified by the user but must be there
		ar.ReadString (S               );

		// Datum settings
		// Note - for normal maps only the datum in the first field is used
		// the rest of the parameters are for datum shifts
		// and there is normally no need for these to be modified by the user
		ar.ReadString (S               );

		// Reserved fields, the lines must be there
		ar.ReadString (S               ); // "Reserved 1"
		ar.ReadString (S               ); // "Reserved 2"

		// Magnetic Variation entry - degrees,minutes
		ar.ReadString (S               );
		sscanf((LPCTSTR)S,"%[^','],%[^','],%[^','],%[^',']",
			S1 .GetBufferSetLength(255),
			S2 .GetBufferSetLength(255),
			S3 .GetBufferSetLength(255),
			S12.GetBufferSetLength(255));
		S2.TrimLeft();
		S3.TrimLeft();
		MagneticVariation=atoi(S2)+atof(S3)/60;
		S12.TrimLeft();
		S12.TrimRight();
		if(S12=="W"||S12=="w")
		{MagneticVariation=-MagneticVariation;}
		else if(S12!="E"&&S12!="e"&&S12!="")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}

		// Map Projection
		// Parameters:
		// 1. Map Projection - must match the projection name used in OziExplorer 
		// 2. PolyCal - the next parameter is Yes if Polynomial calibration is used and No if not 
		// 3. AutoCalOnly - Yes if calibration cannot be adjusted by the user No if it can 
		// 4. BSBUseWPX - For BSB images only, Yes if the calibration equations contained in the BSB file are used. 
		ar.ReadString (S               );

		sscanf((LPCTSTR)S,"%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^',']",
			S21.GetBufferSetLength(255),
			S22.GetBufferSetLength(255),
			S23.GetBufferSetLength(255),
			S24.GetBufferSetLength(255),
			S25.GetBufferSetLength(255),
			S26.GetBufferSetLength(255),
			S27.GetBufferSetLength(255),
			S28.GetBufferSetLength(255));
		S21.TrimLeft();
		S21.TrimRight();
		if(S21!="Map Projection")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}
		S22.TrimLeft();
		S22.TrimRight();
		Projection=S22;
		S23.TrimLeft();
		S23.TrimRight();
		if(S23!="PolyCal")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}
		S24.TrimLeft();
		S24.TrimRight();
		if(S24=="Yes"||S24=="yes"||S24=="YES")
		{PolyCal=true;}
		else if(S24=="No"||S24=="no"||S24=="NO"||S24=="")
		{PolyCal=false;}
		else
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}
		S25.TrimLeft();
		S25.TrimRight();
		if(S25!="AutoCalOnly")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}
		S26.TrimLeft();
		S26.TrimRight();
		if(S26=="Yes"||S26=="yes"||S26=="YES")
		{AutoCalOnly=true;}
		else if(S26=="No"||S26=="no"||S26=="NO"||S26=="")
		{AutoCalOnly=false;}
		else
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}
		S27.TrimLeft();
		S27.TrimRight();
		if(S27!="BSBUseWPX")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}
		S28.TrimLeft();
		S28.TrimRight();
		if(S28=="Yes"||S28=="yes"||S28=="YES")
		{BSBUseWPX=true;}
		else if(S28=="No"||S28=="no"||S28=="NO"||S28=="")
		{BSBUseWPX=false;}
		else
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}

		// Calibration points, there are always 30
		// Only 9 points can be modified from within OziExplorer
		// but OziExplorer will use the full 30 if they exist in the .map file
		for(int i=0;i<MaxCalibrationPoints;i++)
		{
			CalPoint[i].Serialize(ar, i+1);
		}

		// Projection setup parmeters, all on one line.
		ar.ReadString (S               );

		sscanf((LPCTSTR)S,"%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^',']",
			S21.GetBufferSetLength(255),
			S22.GetBufferSetLength(255),
			S23.GetBufferSetLength(255),
			S24.GetBufferSetLength(255),
			S25.GetBufferSetLength(255),
			S26.GetBufferSetLength(255),
			S27.GetBufferSetLength(255),
			S28.GetBufferSetLength(255));
		S21.TrimLeft();
		S21.TrimRight();
		if(S21!="Projection Setup")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}

		// A marker line, it contains no information but is used as a file marker, must be there
		ar.ReadString (S               );

		S.TrimLeft();
		S.TrimRight();
		if(S!="Map Feature = MF ; Map Comment = MC     These follow if they exist")
		{// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
		}

		while(1)
		{
			ar.ReadString (S               );
			S.TrimLeft();
			S.TrimRight();
			if(S=="Track File = TF      These follow if they exist")break;
			if(S.Left(2)=="MC")
			{ //Map Comment
				//...
				sscanf((LPCTSTR)S,"MC,%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^','],%[^',']",
					S12 .GetBufferSetLength(255),

					S21 .GetBufferSetLength(255),
					S22 .GetBufferSetLength(255),

					S23 .GetBufferSetLength(255),

					S24 .GetBufferSetLength(255),
					S25 .GetBufferSetLength(255),

					S26 .GetBufferSetLength(255),
					S27 .GetBufferSetLength(255),

					S28 .GetBufferSetLength(255),
					S29 .GetBufferSetLength(255));

				S12.TrimLeft();
				S12.TrimRight();
				i=atoi(S12);

				S21.TrimLeft();
				S21.TrimRight();
				S22.TrimLeft();
				S22.TrimRight();

				// 2-nd string:
				ar.ReadString (S               );
				S.TrimLeft();
				S.TrimRight();
				SetMapComment(CMapComment(CGeoPoint(atof(S21),atof(S22)),S),i);

				S24.TrimLeft();
				S24.TrimRight();
				S25.TrimLeft();
				S25.TrimRight();

				MapComment[i-1].SetColors(atoi(S24),atoi(S25));

				S26.TrimLeft();
				S26.TrimRight();
				S27.TrimLeft();
				S27.TrimRight();

				MapComment[i-1].SetDimensions(atoi(S26),atoi(S27));

				S28.TrimLeft();
				S28.TrimRight();
				S29.TrimLeft();
				S29.TrimRight();

				MapComment[i-1].SetFont(atoi(S28),400+300*atoi(S29));

				continue;
			}
			if(S.Left(2)=="MF")
			{ //Map Feature
				//...

				// 2-nd string:
				ar.ReadString (S               );
				S.TrimLeft();
				S.TrimRight();
				//...

				// 3-d string:
				ar.ReadString (S               );
				S.TrimLeft();
				S.TrimRight();
				//...
				continue;
			}
			// bad format
			MessageBox(NULL,"bad *.map format","",MB_OK|MB_ICONEXCLAMATION);
			break;
		}

		// Attached file marker, .plt, .wpt, .evt, .pnt files can be included

	}
	return;
}

bool CMyMap::SetMapComment(CMapComment MapCt, int iComment) // if iComment==0 then auto-mode
{
	if(iComment>0&&iComment<=MaxMapComments)
	{
		MapComment[iComment-1]=MapCt;
		return(true);
	}
	else
	{
		for(int i=0;i<MaxMapComments;i++)
		{
			if( MapComment[i].isEmpty())
			{
				MapComment[i]=MapCt;
				//MapComment[i].Use(true);
				return(true);
			}
		}
		return(false);
	}
}

CMapComment CMyMap::GetMapComment(int iComment) // [1..MaxMapComments]
{
	CMapComment MapCt;
	if(iComment>0&&iComment<=MaxMapComments)MapCt=MapComment[iComment-1];
	return(MapCt);
}
