// ProAzEqd.h: interface for the CProAzEqd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROAZEQD_H__97D5D73E_6CBC_4C50_B62E_6531F1030181__INCLUDED_)
#define AFX_PROAZEQD_H__97D5D73E_6CBC_4C50_B62E_6531F1030181__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Pro.h"

class CProAzEqd : public CPro  
{
public:
        CProAzEqd();
        CProAzEqd(CMapomDoc* pDoc);
        virtual ~CProAzEqd();

        int    LL2x  (CGeoPoint GP);
        int    LL2y  (CGeoPoint GP);
        double xy2Lat(int x, int y);
        double xy2Lon(int x, int y);

        double CProAzEqd::LL2w(CGeoPoint GP);
        double CProAzEqd::LL2z(CGeoPoint GP);
        double CProAzEqd::wz2Lat(double w, double z);
        double CProAzEqd::wz2Lon(double w, double z);

//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated);
//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3);
//        void OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS);
};

#endif // !defined(AFX_PROAZEQD_H__97D5D73E_6CBC_4C50_B62E_6531F1030181__INCLUDED_)
