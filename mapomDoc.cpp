// mapomDoc.cpp : implementation of the CMapomDoc class
//

#include "stdafx.h"
#include "mapom.h"

#include "mapomDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapomDoc

IMPLEMENT_DYNCREATE(CMapomDoc, CDocument)

BEGIN_MESSAGE_MAP(CMapomDoc, CDocument)
        //{{AFX_MSG_MAP(CMapomDoc)
        ON_WM_COPYDATA()
        //}}AFX_MSG_MAP
END_MESSAGE_MAP()

long WINAPI Modem(long);

/////////////////////////////////////////////////////////////////////////////
// CMapomDoc construction/destruction

CMapomDoc::CMapomDoc()
{
        // TODO: add one-time construction code here

    LARGE_INTEGER m_liTemp; // temp quadword timer value
    if(!QueryPerformanceFrequency(&m_liTemp))
    {
//        m_dFreq=18.2;
        MessageBox(NULL,"Hi-res timin' NOT SUPPORTED","",MB_OK|MB_ICONEXCLAMATION);
    }
    pen0=NULL;
    pen1=NULL;
    pShadowBrush=NULL;
//    DocSz.cx=1024;
//    DocSz.cy=1024;

//    fast_draw =FALSE;
    fast_draw =TRUE;
//      redraw_all=TRUE;

    pMyMap=new CMyMap(this);
    pPro=new CPro(this);

        unsigned long dwThreadId;
        HANDLE hThread;

    hThread = CreateThread( 
        NULL,                        // no security attributes 
        0,                           // use default stack size  
        (LPTHREAD_START_ROUTINE)Modem, // thread function 
//        &dwThrdParam,                // argument to thread function 
        NULL,                        // argument to thread function 
        0,                           // use default creation flags 
        &dwThreadId);                // returns the thread identifier 

//      =FindWindow("MTTTYClass","Multi-threaded TTY");
}

long WINAPI Modem(long lParam)
{
        return(0);
}

CMapomDoc::~CMapomDoc()
{
}

BOOL CMapomDoc::OnNewDocument()
{
    if (!CDocument::OnNewDocument())
            return FALSE;

        // TODO: add reinitialization code here
        // (SDI documents will reuse this document)

    struct _timeb timebuffer;
    _ftime(&timebuffer);
    int real_time_now=timebuffer.time;
    DocCreTime=real_time_now;
    DocModTime=real_time_now;

	delete pMyMap;
	pMyMap=new CMyMap(this);
	delete pPro;
    pPro=new CProLatLon(this);

    SetPens();
    MakeCoefs();
    return TRUE;
}

CMapomView* CMapomDoc::GetView()
{
    POSITION pos=GetFirstViewPosition();
    CView*pFirstView=GetNextView(pos);
    return((CMapomView*)pFirstView);
}

CMainFrame* CMapomDoc::GetMainFrame()
{
    POSITION pos=GetFirstViewPosition();
    CView*pFirstView=GetNextView(pos);
    return((CMainFrame*)(pFirstView->GetParent()));
}

void CMapomDoc::SetPens()
{
    LOGPEN my_pen; // :-)
    my_pen.lopnWidth.x=1;
    my_pen.lopnStyle=PS_SOLID;

    delete pen0;
    pen0=new CPen();
    my_pen.lopnColor=RGB(192,  0,  0);
    pen0->CreatePenIndirect(&my_pen);
    delete pen1;
    pen1=new CPen();
//  my_pen.lopnColor=RGB(  0,  0,192);
    my_pen.lopnColor=RGB(  0,192,  0);
    pen1->CreatePenIndirect(&my_pen);

    delete pShadowBrush;
    pShadowBrush=new CBrush();
    pShadowBrush->CreateHatchBrush(HS_DIAGCROSS,RGB(0,0,0));
}

/////////////////////////////////////////////////////////////////////////////
// CMapomDoc serialization

void CMapomDoc::Serialize(CArchive& ar)
{
    CString SR, SW="OziExplorer Map Data File Version 2.1";
    if (ar.IsStoring())
    {
    // TODO: add storing code here
        // Header and Version of File
        ar.WriteString((LPCTSTR)(SW+"\r\n"));
    }
    else
    {
    // TODO: add loading code here
        // Header and Version of File
        ar.ReadString(SR);
    }
//    MyMap.Serialize(ar);
    pMyMap->Serialize(ar);
    if (!ar.IsStoring())
    {
        if(!pMyMap->LoadPict())
        {
            MessageBeep(MB_ICONHAND);
            MessageBox(NULL,"Can not open picture file...","Sorry!",MB_OK|MB_TASKMODAL|MB_ICONWARNING);
        }
        MakeCoefs();
    }

}

/////////////////////////////////////////////////////////////////////////////
// CMapomDoc diagnostics

#ifdef _DEBUG
void CMapomDoc::AssertValid() const
{
    CDocument::AssertValid();
}

void CMapomDoc::Dump(CDumpContext& dc) const
{
    CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMapomDoc commands

int CMapomDoc::MakeCoefs()
{
	CString P=pMyMap->GetProjectionName();
	if     (P=="Latitude/Longitude")
	{
		delete pPro;
		pPro=new CProLatLon  (this);
	    pPro->Calibrate(pMyMap->GetCalibrationPoint(1), pMyMap->GetCalibrationPoint(2), pMyMap->GetCalibrationPoint(3));
	}
	else if(P=="Mercator")
	{
		delete pPro;
		pPro=new CProMercator(this);
	    pPro->Calibrate(pMyMap->GetCalibrationPoint(1), pMyMap->GetCalibrationPoint(2), pMyMap->GetCalibrationPoint(3));
//	    pPro->Calibrate();
	}
	else if(P=="Azimuthal Equidistant")
	{
		delete pPro;
		pPro=new CProAzEqd   (this);
	    pPro->Calibrate(pMyMap->GetCalibrationPoint(1), pMyMap->GetCalibrationPoint(2), pMyMap->GetCalibrationPoint(3));
	}
//	else if(P=="XXXXXXXX")
//	{
//		pPro=new CProXXXXXXXX(this);
//	}
	else
	{
		delete pPro;
		pPro=new CProUnknown (this);
	    pPro->Calibrate();
	}
//    pPro->Calibrate(pMyMap->GetCalibrationPoint(1), pMyMap->GetCalibrationPoint(2), pMyMap->GetCalibrationPoint(3));
    redraw_all=TRUE;
    UpdateAllViews(NULL);
    return(0);
}


BOOL CMapomDoc::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
        // TODO: Add your message handler code here and/or call default
        
//      return CDocument::OnCopyData(pWnd, pCopyDataStruct);
        return(false);
}
