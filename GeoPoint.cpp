// GeoPoint.cpp: implementation of the CGeoPoint class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "GeoPoint.h"
#include "math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGeoPoint::CGeoPoint()
{
	Lat=0;
	Lon=0;
}

//CGeoPoint::CGeoPoint(float lat, float lon)
//{
//	Lat=(lat>90?90:lat<-90?-90:lat);
//	Lon=lon-360*(int)((lon+180)/360);
//	Lon+=(Lon<-180?360:0);
//}

CGeoPoint::CGeoPoint(double lat, double lon)
{
	Lat=(lat>90?90:lat<-90?-90:lat);
//	Lon=lon-360*(int)((lon+180)/360);
//	Lon+=(Lon<-180?360:0);
	Lon=lon;
	if(180<Lon||Lon<-180)
	{
		Lon=Lon-360*(int)((Lon+180)/360);
		Lon+=(Lon<-180?360:0);
	}
}

//CGeoPoint(CGeoPoint GeoPoint)
//{
//	Lat=GeoPoint.GetLat();
//	Lon=GeoPoint.GetLon();
//}

CGeoPoint::~CGeoPoint()
{
}

BOOL CGeoPoint::Is_N()
{
	return(Lat>0);
}

BOOL CGeoPoint::Is_S()
{
	return(Lat<0);
}

BOOL CGeoPoint::Is_W()
{
	return(Lon<0);
}

BOOL CGeoPoint::Is_E()
{
	return(Lon>0);
}

double CGeoPoint::GetLat()
{
	return(Lat);
}

double CGeoPoint::GetLon()
{
	return(Lon);
}

unsigned int CGeoPoint::GetDegLat()
{
	return(unsigned int)(fabs(Lat));
}

unsigned int CGeoPoint::GetDegLon()
{
	return(unsigned int)(fabs(Lon));
}

double CGeoPoint::GetMinLat()
{
	return(60*(fabs(Lat)-(int)(fabs(Lat))));
}

double CGeoPoint::GetMinLon()
{
	return(60*(fabs(Lon)-(int)(fabs(Lon))));
}

unsigned int CGeoPoint::GetMintLat()
{
	return(unsigned int)(GetMinLat());
}

unsigned int CGeoPoint::GetMintLon()
{
	return(unsigned int)(GetMinLon());
}

double CGeoPoint::GetSecLat()
{
	return(60*(GetMinLat()-GetMintLat()));
}

double CGeoPoint::GetSecLon()
{
	return(60*(GetMinLon()-GetMintLon()));
}

CString CGeoPoint::GetUTM()
{
	char c=' ';
	int i=0;
	CString str;
	if(Lat<-84) // -90<Lat<84
	{
		c=Lon<0?'A':'B';
		str.Format("%c",c);
	}
	else if(Lat<84) // -84<=Lat<84
	{
		i=30+(Lon<0?(int)Lon/6:1+(int)Lon/6);
		if(Lat<-72){c='C';}else
		if(Lat<-64){c='D';}else
		if(Lat<-56){c='E';}else
		if(Lat<-48){c='F';}else
		if(Lat<-40){c='G';}else
		if(Lat<-32){c='H';}else
		if(Lat<-24){c='J';}else
		if(Lat<-16){c='K';}else
		if(Lat<- 8){c='L';}else
		if(Lat<  0){c='M';}else
		if(Lat<  8){c='N';}else
		if(Lat< 16){c='P';}else
		if(Lat< 24){c='Q';}else
		if(Lat< 32){c='R';}else
		if(Lat< 40){c='S';}else
		if(Lat< 48){c='T';}else
		if(Lat< 56){c='U';}else
		if(Lat< 64){c='V';}else
		if(Lat< 72){c='W';}else
			       {c='X';}
		if(c=='V'&&i==31){Lon< 3?i  :i++;}
		if(c=='X'&&i==32){Lon< 9?i--:i++;}
		if(c=='X'&&i==34){Lon<21?i--:i++;}
		if(c=='X'&&i==36){Lon<33?i--:i++;}
		str.Format("%d%c",i,c);
	}
	else // 84<=Lat<90
	{
		c=Lon<0?'Y':'Z';
		str.Format("%c",c);
	}
	return(str);
}
