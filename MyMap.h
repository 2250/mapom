// MyMap.h: interface for the CMyMap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYMAP_H__2A96424A_5135_43FB_8A53_1FF71172DAFD__INCLUDED_)
#define AFX_MYMAP_H__2A96424A_5135_43FB_8A53_1FF71172DAFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Picture.h"
//#include "GeoPoint.h"
#include "CalPoint.h"
#include "MapCmmnt.h"

//drawing flags
#define DrawMap         1
#define DrawGrid        2
//#define DrawMarks       4
#define DrawCalPoints   4
#define DrawComments    8

#define MaxCalibrationPoints 30
#define MaxMapComments 500

class CMapomDoc;

class CMyMap  
{
protected:
    CMapomDoc *pDoc;
    CPicture m_pict; // the picture
	bool Loaded;
	CString Title;
	CString PathName; // path & filename of picture;
	double MagneticVariation; // to West or to East; // Ozi
	CString Projection;
	bool PolyCal;     // Ozi
	bool AutoCalOnly; // Ozi
	bool BSBUseWPX;   // Ozi
	CCalPoint   CalPoint  [MaxCalibrationPoints];
	CMapComment MapComment[MaxMapComments];
//public:
//	bool           use[MaxCalibrationPoints];

public:
    CMyMap();
    CMyMap(CMapomDoc* pDoc);
    //CMyMap(CMapomDoc* pDoc, LPCTSTR pszPathName);
    virtual ~CMyMap();
    bool LoadPict(LPCTSTR pszPathName);
    bool LoadPict();
    void OnDraw(CDC*pDC, int DrawFlags, bool ClientCS);
//    CPicture* GetPicture(){return &m_pict;}
	CSize GetSize();
	bool      SetCalibrationPoint(CCalPoint CalPt, int iPoint=0);
	CCalPoint GetCalibrationPoint(                 int iPoint  );
	bool        SetMapComment    (CMapComment MapCt, int iComment=0);
	CMapComment GetMapComment    (                   int iComment  );
	void      SetTitle           (CString T){Title=T;} // CR\LF not allowed in title string!!
	CString   GetTitle           (         ){return(Title);}
	void      SetProjectionName  (CString P){Projection=P;} // CR\LF not allowed!!
	CString   GetProjectionName  (         ){return(Projection);}
	void Serialize(CArchive& ar);
};

#endif // !defined(AFX_MYMAP_H__2A96424A_5135_43FB_8A53_1FF71172DAFD__INCLUDED_)
