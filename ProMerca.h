// ProMerca.h: interface for the CProMercator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROMERCA_H__B9B3FE39_4D1D_4156_9C63_B6D01CFCEDDA__INCLUDED_)
#define AFX_PROMERCA_H__B9B3FE39_4D1D_4156_9C63_B6D01CFCEDDA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Pro.h"

class CProMercator : public CPro  
{
//protected:
//    double k[6];
//    double r[6];

public:
        CProMercator();
		CProMercator(CMapomDoc* pDoc);
        virtual ~CProMercator();

        int    LL2x  (CGeoPoint GP);
        int    LL2y  (CGeoPoint GP);
        double xy2Lat(int x, int y);
        double xy2Lon(int x, int y);

        double CProMercator::LL2z(CGeoPoint GP);
        double CProMercator::wz2Lat(double w, double z);
//        double CProMercator::Lat2z(double Lat);
  //      double CProMercator::z2Lat(double z);

//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, bool rotated);
//        bool Calibrate(CCalPoint CP1, CCalPoint CP2, CCalPoint CP3);
        void OnDraw(CDC*pDC, CMapomDoc *pDoc, bool ClientCS);
};

#endif // !defined(AFX_PROMERCA_H__B9B3FE39_4D1D_4156_9C63_B6D01CFCEDDA__INCLUDED_)
