// NMEA0183.cpp: implementation of the CNMEA0183 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mapom.h"
#include "NMEA0183.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNMEA0183::CNMEA0183()
{
CheckSum0=-1;
CheckSum1=-1;
Msg=""+((char)13)+((char)10);
}

CNMEA0183::CNMEA0183(CString S)
{
CheckSum0=-1;
CheckSum1=-1;
Msg=S;
}

int CNMEA0183::GetType()
{
	if("$GPGGA"==Msg.Left(6)){return(GPGGA);}
	if("$GPGLL"==Msg.Left(6)){return(GPGLL);}
	if("$GPGSA"==Msg.Left(6)){return(GPGSA);}
	if("$GPGSV"==Msg.Left(6)){return(GPGSV);}
	if("$GPZDA"==Msg.Left(6)){return(GPZDA);}
	return(0);
}

CNMEA0183::~CNMEA0183()
{

}
